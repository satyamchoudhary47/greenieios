import { walletPageRoutingModule } from './wallet-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';


import { walletPage } from './wallet.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    walletPageRoutingModule
  ],
  declarations: [walletPage]
})
export class walletPageModule {}
