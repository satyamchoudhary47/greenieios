import { async } from "@angular/core/testing";
import { Component, OnInit, ViewChild } from "@angular/core";
import { AlertController, ModalController } from "@ionic/angular";
import { CommonService } from "../common.service";
import { Pipe, PipeTransform } from "@angular/core";
import * as moment from "moment";
import { SuccesAndFailComponent } from "../succes-and-fail/succes-and-fail.component";

declare var RazorpayCheckout: any;

@Component({
  selector: "app-tab3",
  templateUrl: "./wallet.page.html",
  styleUrls: ["./wallet.page.scss"],
})
export class walletPage implements OnInit {
  TodayDate = Date();
  walletHistory: any = [];
  userBalance: any;

  number = "1000";

  // month = parseInt(moment(new Date()).format("MM"));
  // year = parseInt(moment(new Date()).format("YYYY"));

  maxDate=new Date().toISOString()



  month= parseInt(moment(new Date()).format('MM'))
  year= parseInt(moment(new Date()).format('YYYY'))

  @ViewChild("dateTime") sTime;


  constructor(
    public alertController: AlertController,
    public modalController: ModalController,
    public service: CommonService
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    console.log("ionViewDidEnter called");
    this.service.getUserBalance(this.month, this.year);
  }


  selectDate(data){
    console.log("data",data)
    console.log("data",moment(data).format('MM'))
    console.log("data",moment(data).format('YYYY'))

    this.month =  parseInt(moment(data).format('MM'))
    this.year =  parseInt(moment(data).format('YYYY'))


    this.ionViewDidEnter();

  }

  async openDate() {
    this.sTime.open();

  }

  async addMoney() {
    console.log("add monmey");
    // const alert = await this.alertController.create({
    //   cssClass: "alertsHeading",
    //   header: "Pay Money",
    //   inputs: [
    //     {
    //       name: "name1",
    //       type: "number",
    //       disabled: true,
    //       placeholder: this.service.userBalance,
    //     },
    //   ],
      // buttons: [
      //   {
      //     text: "Cancel",
      //     role: "cancel",
      //     cssClass: "ion-color-secondary",
      //     handler: () => {
      //       console.log("Confirm Cancel");
      //     },
      //   },
      //   {
      //     text: "Pay",
      //     cssClass: "btnWarning",
      //     handler: (alt) => {
      //       console.log("Confirm amount", alt.name1);
            this.DoPayment(this.service.userBalance);
    //       },
    //     },
    //   ],
    // });

    // await alert.present();
  }

  DoPayment(amount) {
    // console.log("start payment", RazorpayCheckout)
    var obj = this;
    var options = {
      description: "Add Money to your Wallet",
      image:
        "src/assets/blueIcon.png",
      currency: "INR",
      key: "rzp_test_oDw757e5auoleh",
      // key: 'rzp_test_UEQcybnkm10wNp',
      amount: Number(amount * 100),

      name: "Greenie",
      prefill: {
        email: "gaurav.kumar@example.com",
        contact: "9999999999",
      },
      theme: {
        color: "#447F08",
      },
    };

    var successCallback = async function (success) {
      alert("payment_id: " + success.razorpay_payment_id);
      console.log("payment id", success.razorpay_payment_id);
      await obj.service.ClearDues(success.razorpay_payment_id);

      setTimeout(() => {
        obj.ionViewDidEnter();
      }, 500);
      // var orderId = success.razorpay_order_id
      // var signature = success.razorpay_signature
    };
    var cancelCallback = function (error) {};
    RazorpayCheckout.on("payment.success", successCallback);
    RazorpayCheckout.on("payment.cancel", cancelCallback);
    RazorpayCheckout.open(options);
  }

  async openModal(data) {
    console.log("modal data", data);
    const modal = await this.modalController.create({
      component: SuccesAndFailComponent,
      backdropDismiss: true,
      swipeToClose: true,
      //  cssClass: "my-custom-modal-cssforDevice",
      cssClass: "my-custom-modal-cssforTransaction",
      componentProps: { data: data },
      // componentProps: { data: 'success' },
    });
    await modal.present();
  }
}
