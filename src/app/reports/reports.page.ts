import { ViewChild } from "@angular/core";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AlertController, ModalController } from "@ionic/angular";
import { CommonService } from "../common.service";
import { FilterPage } from "../filter/filter.page";
import * as moment from 'moment';

@Component({
  selector: "app-reports",
  templateUrl: "./reports.page.html",
  styleUrls: ["./reports.page.scss"],
})
export class ReportsPage implements OnInit {
  @ViewChild("dateTime") sTime;

  data: any;
  TodayDate = new Date();

  month= parseInt(moment(new Date()).format('MM'))
  year= parseInt(moment(new Date()).format('YYYY'))

  maxDate=new Date().toISOString()

  constructor(
    private route: ActivatedRoute,
    public modalController: ModalController,
    public service:CommonService,
    public alertController:AlertController,
    private router: Router
  ) {
    this.route.queryParams.subscribe((params) => {
      if (params && params.data) {
        this.data = JSON.parse(params.data);
        console.log("data", this.data);
      }
    });
  }

  ngOnInit() {

    this.service.getHostReport(this.month,this.year,this.data.DeviceName)

  }

  selectDate(data){
    console.log("data",data)
    console.log("data",moment(data).format('MM'))
    console.log("data",moment(data).format('YYYY'))

    this.month =  parseInt(moment(data).format('MM'))
    this.year =  parseInt(moment(data).format('YYYY'))


    this.ngOnInit();

  }



  async openDate() {
    this.sTime.open();

    // const modal = await this.modalController.create({
    //   component: FilterPage,
    //   backdropDismiss: false,
    //   animated: true,

    //   cssClass: "my-custom-modal-cssforReports",
    //   // componentProps: { data,
    // });
    // await modal.present();

    // const { data } = await modal.onWillDismiss();
    // if(data.startDate != undefined && data.endDate != undefined){
    //   console.log(data);
    // }
  }

  async export() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Export Reports as PDF ?',
      // message: 'Message <strong>text</strong>!!!',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          cssClass: 'btnWarning',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }

}
