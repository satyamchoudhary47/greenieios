import { Component, OnInit, ViewChild } from "@angular/core";
import { CommonService } from "../common.service";
import { HttpClient } from "@angular/common/http";
import {
  IonInfiniteScroll,
  NavController,
  PopoverController,
} from "@ionic/angular";
import { Storage } from "@ionic/storage";
// import {
//   NativeGeocoder,
//   NativeGeocoderResult,
//   NativeGeocoderOptions,
// } from "@ionic-native/native-geocoder/ngx";

import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@awesome-cordova-plugins/native-geocoder/ngx';

import { NavigationExtras, Router } from "@angular/router";
import { PopoverComponent } from "../popover/popover.component";
import { Popover2Component } from "../popover2/popover2.component";

@Component({
  selector: "app-tab4",
  templateUrl: "./device.page.html",
  styleUrls: ["./device.page.scss"],
})
export class devicePage {
  showList = false;
  devices: any;
  location: any;
  task: any;
  topLimit: number = 5;
  subList: any = [];

  geolist = [];

  USerHistory: any = [];
  constructor(
    public service: CommonService,
    public navCtrl: NavController,
    public storage: Storage,
    public http: HttpClient,
    public popoverController: PopoverController,
    public router: Router,
    public nativeGeocoder: NativeGeocoder
  ) {}

  ionViewDidLoad(){
  }

  ionViewDidEnter() {
    if (this.service.networkIsConnected) {
      var thi = this;
      console.log("user" + JSON.stringify(this.service.currentUser));
      // this.service.getUserOwnedDevices();
    } else {
      this.storage.get("mydevices").then((res: any) => {
        this.devices = res;
        console.log("devices from my storage", JSON.stringify(this.devices));
        // this.http
        //   .get(
        //     "https://us1.locationiq.com/v1/reverse.php?key=pk.9b6a50fa3f1a5bb3b86a43fd033001b1&format=json&lat=" +
        //       this.devices[0].LAT +
        //       "&lon=" +
        //       this.devices[0].LON
        //   )
        //   .subscribe(
        //     (res: any) => {
        //       console.log("get address worked Worked " + JSON.stringify(res));
        //       this.location = res.display_name;
        //     },
        //     (err) => {
        //       console.log("getReverGeoCoding failed" + JSON.stringify(err));
        //     }
        //   );
      });
    }
  }

  report(data) {
    console.log("data", data);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify(data),
      },
    };
    this.router.navigate(["reports"], navigationExtras);
  }

  loadData(event) {
    setTimeout(() => {
      console.log("Done");
      this.topLimit += 8;
      this.subList = this.USerHistory.slice(0, this.topLimit);

      event.target.complete();

      if (this.subList.length == this.USerHistory.length) {
        event.target.disabled = true;
      }
    }, 500);
  }

  async edit(data) {
    console.log("data", data);

    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify(data),
      },
    };

    const popover = await this.popoverController.create({
      component: PopoverComponent,
      translucent: true,
      mode: 'md',
    });
    await popover.present();

    await popover.onDidDismiss().then((res) => {
      console.log("onDidDismiss resolved with role", res.data);
      if (res.data == "detail") {
        this.router.navigate(["reports"], navigationExtras);
      } 
      // else if (res.data == "bank") {
      //   this.router.navigate(["edit-bank"], navigationExtras);
      // } 
      else if (res.data == "edit") {
        this.router.navigate(["edit-devices"], navigationExtras);
      }
      else if (res.data == "wifi") {
        this.router.navigate(["wifi"], navigationExtras);
      }
    });
  }

  viewLess() {
    this.showList = false;
  }


  async EditDevices(){
    const popover = await this.popoverController.create({
      component: Popover2Component,
      translucent: true,
      mode: 'md',
    });
    await popover.present();

    await popover.onDidDismiss().then((res) => {
      console.log("onDidDismiss resolved with role", res.data);
     if (res.data == "bank") {
        this.router.navigate(["edit-bank"]);
      } else if (res.data == "add") {
        this.router.navigate(["add-charger"]);
      }
    });

  }

  showHistory() {
    this.showList = true;

    setTimeout(() => {
      if (this.devices.length > 0) {
        this.service.getUserHistory(this.devices[0].DeviceName);
      }
    }, 500);
    setTimeout(() => {
      this.USerHistory = this.service.userHistory.LIST;
      this.subList = this.USerHistory.slice(0, this.topLimit);

      // this.service.dismissLoading();
      console.log("history array", JSON.stringify(this.USerHistory));
    }, 900);
  }
}

// reverseGeocode() {
//   let options: NativeGeocoderOptions = {
//     useLocale: true,
//     maxResults: 5,
//   };

//   this.nativeGeocoder
//     .reverseGeocode(19.3137989, 73.0656204, options)
//     .then((result: NativeGeocoderResult[]) => {
//       console.log("geocoder reislt" + JSON.stringify(result));

//       this.geolist = result;

//       console.log("geolist", this.geolist);
//     })
//     .catch((error: any) => console.log(error));

//   return this.geolist[0];
// }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// setTimeout(() => {
// this.devices = this.service.devicesOwned
// this.devices.forEach(element => {
//   element.PRICE = element.PRICE * element.TOTALC1
// });
// console.log("devices at tabs 4" + JSON.stringify(this.devices))

// this.storage.set("mydevices", this.devices)
// this.http.get("https://us1.locationiq.com/v1/reverse.php?key=pk.9b6a50fa3f1a5bb3b86a43fd033001b1&format=json&lat=" + this.devices[0].LAT + "&lon=" + this.devices[0].LON).subscribe((res: any) => {
//   console.log("get address worked Worked " + JSON.stringify(res))
//   this.location = res.display_name
// }, (err) => {
//   console.log("getReverGeoCoding failed" + JSON.stringify(err))
// })
// }, 500)
