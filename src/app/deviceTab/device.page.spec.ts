import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { devicePage } from './device.page';

describe('devicePage', () => {
  let component: devicePage;
  let fixture: ComponentFixture<devicePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ devicePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(devicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
