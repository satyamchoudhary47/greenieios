import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { devicePage } from './device.page';

const routes: Routes = [
  {
    path: '',
    component: devicePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class devicePageRoutingModule {}
