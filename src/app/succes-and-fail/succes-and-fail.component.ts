import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-succes-and-fail',
  templateUrl: './succes-and-fail.component.html',
  styleUrls: ['./succes-and-fail.component.scss'],
})
export class SuccesAndFailComponent implements OnInit {

  data:any

  constructor(public modal:ModalController) { }

  ngOnInit() {
    console.log('data from parent',this.data)
  }

  close(){
    this.modal.dismiss({})
  }

}
