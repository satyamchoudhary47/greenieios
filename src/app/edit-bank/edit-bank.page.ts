import { Location } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { CommonService } from "../common.service";

@Component({
  selector: "app-edit-bank",
  templateUrl: "./edit-bank.page.html",
  styleUrls: ["./edit-bank.page.scss"],
})
export class EditBankPage implements OnInit {
  accNumber: any;
  confirmAccNumber: any;
  ifsc: any;
  nameOnbank: any;
  nameOfBank = "Bank Of Baroda";
  locationOfBank = "Marol";
  locationOfCity = "Mumbai";

  cvv: any = 123;

  constructor(public location: Location, public service: CommonService) {}

  ngOnInit() {}

  submit() {
    if (this.accNumber == this.confirmAccNumber) {
      this.location.back();
    } else {
      this.service.showToast(
        "Account Number and Confirm Account number Does not match"
      );
    }
  }
}
