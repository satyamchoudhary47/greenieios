import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditDevicesPageRoutingModule } from './edit-devices-routing.module';

import { EditDevicesPage } from './edit-devices.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditDevicesPageRoutingModule
  ],
  declarations: [EditDevicesPage]
})
export class EditDevicesPageModule {}
