import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-edit-devices',
  templateUrl: './edit-devices.page.html',
  styleUrls: ['./edit-devices.page.scss'],
})
export class EditDevicesPage implements OnInit {
  rate:any = '12'

  data:any

  constructor(public location:Location,
    public service:CommonService,
    private route: ActivatedRoute,
    ) { 

    this.route.queryParams.subscribe((params) => {
      if (params && params.data) {
        this.data = JSON.parse(params.data);
        console.log("data", this.data);
        this.rate =this.data.PRICE
      }
    });
  }

  ngOnInit() {
  }

  submit(){

    if(this.rate != this.data.PRICE){
      this.service.changePrice(this.rate,this.data.DeviceName)
    }
    else{
      this.service.showToast('Please Change the Price First')
    }




    // this.location.back()
  } 

}
