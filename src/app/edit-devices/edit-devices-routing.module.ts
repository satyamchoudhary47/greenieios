import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditDevicesPage } from './edit-devices.page';

const routes: Routes = [
  {
    path: '',
    component: EditDevicesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditDevicesPageRoutingModule {}
