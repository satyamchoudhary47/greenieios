import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-intial-scan',
  templateUrl: './intial-scan.page.html',
  styleUrls: ['./intial-scan.page.scss'],
})
export class IntialScanPage implements OnInit {

  constructor(public router: Router,
    public navctrl: NavController) { }

  ngOnInit() {
  }

  scan() {

    // this.barcodeScanner.scan().then(barcodeData => {
    //   console.log('Barcode data', barcodeData);
    // }).catch(err => {
    //   console.log('Error', err);
    // });
  }

  home() {
    this.navctrl.setDirection('root');
    this.router.navigateByUrl('/tabs')
  }

}
