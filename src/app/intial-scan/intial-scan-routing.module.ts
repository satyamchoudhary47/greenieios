import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IntialScanPage } from './intial-scan.page';

const routes: Routes = [
  {
    path: '',
    component: IntialScanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IntialScanPageRoutingModule {}
