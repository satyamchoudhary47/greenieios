import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IntialScanPageRoutingModule } from './intial-scan-routing.module';

import { IntialScanPage } from './intial-scan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IntialScanPageRoutingModule
  ],
  declarations: [IntialScanPage]
})
export class IntialScanPageModule {}
