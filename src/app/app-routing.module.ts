import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },  {
    path: 'intial-scan',
    loadChildren: () => import('./intial-scan/intial-scan.module').then( m => m.IntialScanPageModule)
  },
  {
    path: 'flash',
    loadChildren: () => import('./flash/flash.module').then( m => m.FlashPageModule)
  },
  {
    path: 'device-on-map',
    loadChildren: () => import('./device-on-map/device-on-map.module').then( m => m.DeviceOnMapPageModule)
  },
  {
    path: 'reports',
    loadChildren: () => import('./reports/reports.module').then( m => m.ReportsPageModule)
  },
  {
    path: 'filter',
    loadChildren: () => import('./filter/filter.module').then( m => m.FilterPageModule)
  },
  {
    path: 'edit-devices',
    loadChildren: () => import('./edit-devices/edit-devices.module').then( m => m.EditDevicesPageModule)
  },
  {
    path: 'edit-bank',
    loadChildren: () => import('./edit-bank/edit-bank.module').then( m => m.EditBankPageModule)
  },
  {
    path: 'add-charger',
    loadChildren: () => import('./add-charger/add-charger.module').then( m => m.AddChargerPageModule)
  },
  {
    path: 'user',
    loadChildren: () => import('./user/user.module').then( m => m.UserPageModule)
  },
  {
    path: 'host-wallet',
    loadChildren: () => import('./host-wallet/host-wallet.module').then( m => m.HostWalletPageModule)
  },
  {
    path: 'wifi',
    loadChildren: () => import('./wifi/wifi.module').then( m => m.WifiPageModule)
  },
  {
    path: 'slider',
    loadChildren: () => import('./slider/slider.module').then( m => m.SliderPageModule)
  },
  {
    path: 'my-profile',
    loadChildren: () => import('./my-profile/my-profile.module').then( m => m.MyProfilePageModule)
  },
  {
    path: 'get-email',
    loadChildren: () => import('./get-email/get-email.module').then( m => m.GetEmailPageModule)
  },

  // {
  //   path: 'tab1',
  //   loadChildren: () => import('./tab1/tab1.module').then( m => m.Tab1PageModule)
  // },
  // {
  //   path: 'tab2',
  //   loadChildren: () => import('./tab2/tab2.module').then( m => m.Tab2PageModule)
  // },
  // {
  //   path: 'tab3',
  //   loadChildren: () => import('./tab3/tab3.module').then( m => m.Tab3PageModule)
  // },
  // {
  //   path: 'tab4',
  //   loadChildren: () => import('./tab4/tab4.module').then( m => m.Tab4PageModule)
  // },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
