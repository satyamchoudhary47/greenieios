import { Component, OnInit } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { CommonService } from "../common.service";

@Component({
  selector: "app-get-email",
  templateUrl: "./get-email.page.html",
  styleUrls: ["./get-email.page.scss"],
})
export class GetEmailPage implements OnInit {
  email: any;
  name: any;

  constructor(public modal: ModalController, public common: CommonService) {}

  ngOnInit() {}

  submit() {
    if (this.email && this.name) {
      if (/(.+)@(.+){2,}\.(.+){2,}/.test(this.email)) {
        this.modal.dismiss({
          email: this.email,
          name: this.name,
        });
      } else {
        this.common.showToast("Please enter a valid email id");
      }
    } else {
      this.common.showToast("Please fill all the details");
    }
  }
}
