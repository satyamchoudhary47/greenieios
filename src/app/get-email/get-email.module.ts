import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GetEmailPageRoutingModule } from './get-email-routing.module';

import { GetEmailPage } from './get-email.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GetEmailPageRoutingModule
  ],
  declarations: [GetEmailPage]
})
export class GetEmailPageModule {}
