import { Component, Input, OnInit } from "@angular/core";
// import { Geolocation } from "@ionic-native/geolocation/ngx";
import { Geolocation } from '@awesome-cordova-plugins/geolocation/ngx';
import { ModalController, Platform } from "@ionic/angular";
// import {
//   LaunchNavigator,
//   LaunchNavigatorOptions,
// } from "@ionic-native/launch-navigator/ngx";
import { LaunchNavigator, LaunchNavigatorOptions } from '@awesome-cordova-plugins/launch-navigator/ngx';


@Component({
  selector: "app-device-on-map",
  templateUrl: "./device-on-map.page.html",
  styleUrls: ["./device-on-map.page.scss"],
})
export class DeviceOnMapPage implements OnInit {
  @Input() data: any;

  latitude: any;
  longitude: any;

  constructor(
    public modal: ModalController,
    public launchNavigator: LaunchNavigator,
    public platform: Platform,
    public geolocation: Geolocation
  ) {
    this.platform.ready().then(() => {
      //set options..
      var options = {
        enableHighAccuracy: true,
        timeout: 60000,
        maximumAge: 0,
      };

      this.geolocation.getCurrentPosition(options).then(
        (position) => {
          console.log("Posotion", position);

          this.latitude = position.coords.latitude;
          this.longitude = position.coords.longitude;

          console.log(
            "latitude>>",
            this.latitude + " " + "longitude>>",
            this.longitude
          );
        },
        (error) => {
          console.log("error", error);
        }
      );
    });
  }

  ngOnInit() {
    console.log("data on modal page", this.data);

    console.log("watch current position");
  }

  ionViewDidEnter() {}

  close() {
    this.modal.dismiss();
  }

  navTodevice() {
    console.log("navigate to google");
    console.log(
      "current location",
      this.latitude + "  " + "long" + this.longitude
    );
    console.log(
      "navigate to destination",
      this.data.LAT + "long" + " " + this.data.LON
    );
    let options: LaunchNavigatorOptions = {
      start: [this.latitude, this.longitude],
      app: this.launchNavigator.APP.GOOGLE_MAPS,
    };
    this.launchNavigator.navigate([this.data.LAT, this.data.LON], options).then(
      (success) => {
        console.log(success);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
