import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DeviceOnMapPage } from './device-on-map.page';

describe('DeviceOnMapPage', () => {
  let component: DeviceOnMapPage;
  let fixture: ComponentFixture<DeviceOnMapPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceOnMapPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DeviceOnMapPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
