import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddChargerPage } from './add-charger.page';

const routes: Routes = [
  {
    path: '',
    component: AddChargerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddChargerPageRoutingModule {}
