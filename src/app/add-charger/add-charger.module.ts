import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddChargerPageRoutingModule } from './add-charger-routing.module';

import { AddChargerPage } from './add-charger.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AddChargerPageRoutingModule
  ],
  declarations: [AddChargerPage]
})
export class AddChargerPageModule {}
