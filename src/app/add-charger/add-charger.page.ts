import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-add-charger',
  templateUrl: './add-charger.page.html',
  styleUrls: ['./add-charger.page.scss'],
})
export class AddChargerPage implements OnInit {


  ionicForm: FormGroup;
  isSubmitted = false;

  constructor(public formBuilder: FormBuilder,public location:Location,public service:CommonService) { }

  ngOnInit() {
    this.ionicForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      mobile: [this.service.mobileNumber, [Validators.required, Validators.pattern('^[0-9]+$')]],
      address:  ['', [Validators.required, Validators.minLength(2)]],
    })
  }

  

  get errorControl() {
    return this.ionicForm.controls;
  }

  submitForm() {
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      console.log(this.ionicForm.value)

      this.service.installCharger(this.ionicForm.value)
    }
  }
}
