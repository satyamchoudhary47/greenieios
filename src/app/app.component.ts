import { Component } from "@angular/core";
import { MenuController, NavController, Platform } from "@ionic/angular";
// import { SplashScreen } from "@ionic-native/splash-screen/ngx";
// import { SplashScreen } from '@awesome-cordova-plugins/splash-screen/ngx';
// import { StatusBar } from "@ionic-native/status-bar/ngx";
// import { StatusBar } from '@awesome-cordova-plugins/status-bar/ngx';

import { CommonService } from "./common.service";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
// import { Push, PushOptions, PushObject } from "@ionic-native/push/ngx";
// import { LottieSplashScreen } from "@ionic-native/lottie-splash-screen/ngx";
// import { Network } from "@ionic-native/network/ngx";
import { Network } from '@awesome-cordova-plugins/network/ngx';

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent {
  ws: any;

  toggleValue: any;

  constructor(
    private platform: Platform,
    public storage: Storage,
    // private splashScreen: SplashScreen,
    // private push: Push,
    private router: Router,
    public service: CommonService,
    private network: Network,
    private navCtrl: NavController,
    // private lottieSplashScreen: LottieSplashScreen,
    public menu: MenuController,
    // private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  closeMenu() {
    this.menu.close();
  }

  async initializeApp() {
    await this.storage.create();
    this.platform.ready().then(() => {
      // setTimeout(() => {
      //   this.lottieSplashScreen.hide();
      // }, 2500);
      // this.getToken();
      this.checkForUserInStorage();

      // this.statusBar.backgroundColorByHexString("#6a981c");

      // this.statusBar.overlaysWebView(false);
      // this.splashScreen.hide();

      this.checkForNetwork();
    });
  }

  checkForNetwork() {
    this.network.onDisconnect().subscribe(() => {
      console.log("network was disconnected :-(");
      // alert("disconneted")
      this.service.networkIsConnected = false;
    });

    // stop disconnect watch

    // watch network for a connection
    this.network.onConnect().subscribe(() => {
      console.log("network connected!");
      // alert("connected")

      if(this.service.networkIsConnected ==false){
        alert('Your are back online please reload the app')
        navigator['app'].exitApp();
      }

      this.service.networkIsConnected = true;

      // this.service.startSocketttt();
      this.service.connectToSocket(true);
    });

    // stop connect watch
  }

  async checkForUserInStorage() {
    await this.storage.get("currentUser").then(async (res) => {
      console.log("user after" + JSON.stringify(res));

      if (res != null) {
        console.log("number", res);

        this.service.mobileNumber = res;
        // this.service.startSocketttt();
        this.service.connectToSocket(true);

        // this.navCtrl.navigateRoot("root");
        // this.router.navigateByUrl("/tabs");
      } else {
        console.log("storage is null setting to root page");
        this.navCtrl.navigateRoot("root");
        // this.router.navigateByUrl("/login");
        this.router.navigateByUrl("/slider");
        // this.service.startSocket();
        this.service.connectToSocket(false);
      }
    });
  }

  async startSocketAndRoute() {
    const item = await this.service.startSocket();
    console.log("startSocket res appcomp; ", item);
    if (item == true) {
      this.navCtrl.navigateRoot("root");
      this.router.navigateByUrl("/tabs");
    }
  }

  logout() {
    this.service.logout();
  }

  toggleUser() {
    console.log("toggle value", this.toggleValue);

    // this.toggleValue = ! this.toggleValue

    if (this.toggleValue) {
      this.service.isUser = true;
    } else {
      this.service.isUser = false;
    }
  }

  // getToken() {
  //   console.log("cs");

  //   const options: PushOptions = {
  //     android: { senderID: "636472206040" },
  //     ios: {
  //       alert: "true",
  //       badge: true,
  //       sound: "false",
  //     },
  //     windows: {},
  //     browser: {
  //       pushServiceURL: "http://push.api.phonegap.com/v1/push",
  //     },
  //   };

  //   const pushObject: PushObject = this.push.init(options);

  //   pushObject.on("notification").subscribe((notification: any) => {
  //     console.log("Received a notification", notification);
  //     alert(notification.title);
  //   });

  //   pushObject.on("registration").subscribe((res) => {
  //     console.log("device id", res.registrationId);
  //     // alert("device Id>>"+res.registrationId)
  //     this.service.deviceId = res.registrationId;
  //   });

  //   pushObject
  //     .on("error")
  //     .subscribe((error) => console.error("Error with Push plugin", error));
  // }
}
