import { Location } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { AlertController } from "@ionic/angular";
import { CommonService } from "../common.service";

@Component({
  selector: "app-wifi",
  templateUrl: "./wifi.page.html",
  styleUrls: ["./wifi.page.scss"],
})
export class WifiPage implements OnInit {
  data: any;

  wifiResponse: any 

  constructor(
    public location: Location,
    public service: CommonService,
    public alertController:AlertController,
    private route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe((params) => {
      if (params && params.data) {
        this.data = JSON.parse(params.data);
        console.log("data", this.data);
      }
    });
  }

  ngOnInit() {
    this.service.getWifiList(this.data.DeviceName).then((res: any) => {
      console.log("res", res);
      if (res) {
        this.wifiResponse = res;
      }
    });
  }

  async change(data) {
    console.log("data", data);

    const alert = await this.alertController.create({
      cssClass: 'alertsHeading',
      header: 'Confirm!',
      message: 'Are you Sure you want to Switch to ' + data.Network,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          cssClass: 'btnWarning',
          handler: () => {
            console.log('Confirm Okay');

            this.askPaasword(data.Network)


          }
        }
      ]
    });

    await alert.present();

   
  }

   async askPaasword(data){

    const alert = await this.alertController.create({
      cssClass: 'alertsHeading',
      header: 'Switch Wifi to ' + data,
      inputs: [
        {
          name: 'name1',
          type: 'password',
          placeholder: 'Enter Password'
        },
       
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          cssClass: 'btnWarning',
          handler: (alt) => {
            console.log('Confirm Ok',alt.name1);
            if(alt.name1){
              this.service
              .switchWifi(data.Network, alt.name1, this.wifiResponse.deviceID)
              .then((res: any) => {
                console.log("res ===", res);
              });
            }
          }
        }
      ]
    });

    await alert.present();
  }


}
