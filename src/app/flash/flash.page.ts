// import { NativeAudio } from "@ionic-native/native-audio/ngx";
import { NativeAudio } from '@awesome-cordova-plugins/native-audio/ngx';

import { async } from "@angular/core/testing";
// import { Brightness } from "@ionic-native/brightness/ngx";

// import { ScreenBrightness } from '@capacitor-community/screen-brightness';
import { ChangeDetectorRef, Component, Input, OnInit } from "@angular/core";
import { ModalController, Platform } from "@ionic/angular";
// import { Vibration } from "@ionic-native/vibration/ngx";
import { Vibration } from '@awesome-cordova-plugins/vibration/ngx';

@Component({
  selector: "app-flash",
  templateUrl: "./flash.page.html",
  styleUrls: ["./flash.page.scss"],
})
export class FlashPage implements OnInit {
  @Input() data: any;
  color: any = "light";

  time = 75;

  timeleft = 3;
  downloadTimer: any;
  show = true;

  count = 0;

  time2 = 100;

  LongDelay = 2000;
  ShortDelay = 1000;

  showButtons = true;

  constructor(
    public modal: ModalController,
    public nativeAudio: NativeAudio,
    public platform: Platform,
    private vibration: Vibration,
    // public brightness: ScreenBrightness
  ) {}

  ngOnInit() {
    // let brightnessValue = 1.0;
    // this.brightness.setBrightness(brightnessValue);

    this.platform.ready().then(() => {
      this.nativeAudio
        .preloadSimple("uniqueId1", "assets/audio/sound.mp3")
        .then(
          (res) => {
            console.log("res", res);
          },
          (err) => {
            console.log("err", err);
          }
        );
    });

    this.showButtons = false;
    var obj = this;
    this.downloadTimer = setInterval(function () {
      if (obj.timeleft <= 0) {
        clearInterval(obj.downloadTimer);
        obj.show = false;

        obj.Execute(obj.data);
        // obj.flashFunc();
      } else {
        document.getElementById("countdown").innerHTML =
          obj.timeleft + " seconds remaining";
      }
      obj.timeleft -= 1;
    }, 1000);
  }

  // startFlash() {
  //   this.showButtons = false;
  //   var obj = this;
  //   this.downloadTimer = setInterval(function () {
  //     if (obj.timeleft <= 0) {
  //       clearInterval(obj.downloadTimer);
  //       obj.show = false;

  //       obj.Execute(obj.data);
  //       // obj.flashFunc();
  //     } else {
  //       document.getElementById("countdown").innerHTML =
  //         obj.timeleft + " seconds remaining";
  //     }
  //     obj.timeleft -= 1;
  //   }, 1000);
  // }

  ionViewDidLeave() {
    console.log("ionViewDidLeave");
    clearInterval();
  }

  async flashFunc() {
    console.log("data", this.data);
    await this.data.forEach((brightnesslevel, i) => {
      setTimeout(() => {
        if (brightnesslevel == ".") {
          console.log("Black color");
          this.color = "dark";
          this.count++;
          this.data.length == this.count ? this.dismissModal() : null;
        } else if (brightnesslevel == "_") {
          console.log("white color");
          this.color = "light";
          this.count++;
          this.data.length == this.count ? this.dismissModal() : null;
        } else if (brightnesslevel == "G") {
          console.log("gray color");
          this.color = "medium";
          this.count++;
          this.data.length == this.count ? this.dismissModal() : null;
        } else if (brightnesslevel == 5) {
          this.time = 5;
          console.log("time", this.time);
          this.count++;
          this.data.length == this.count ? this.dismissModal() : null;
        } else {
          this.time = 25;
          console.log("time", this.time);
          this.count++;
          this.data.length == this.count ? this.dismissModal() : null;
        }
      }, i * this.time);
    });
    console.log("this.data.length", this.data.length);
    // if (this.data.length == this.count) {
    //   console.log("cotnt", this.count)

    //   this.dismissModal()
    // }
    // console.log("this.data.length 2",this.data.length)
  }

  cancel() {
    this.modal.dismiss();
  }

  Execute(MCode) {
    setTimeout(() => {
      console.log("initial white");
      this.color = "light";
      setTimeout(() => {
        console.log("initial black");
        this.color = "dark";

        setTimeout(() => {
          console.log("initial grey");
          this.color = "medium";

          setTimeout(() => {
            console.log("excute  mcode array");
            console.log("MCode", MCode);
            this.executeNext(MCode);
          }, 300); //main code
        }, 300); //gray
      }, 300); // black
    }, 300); //white
  }

  async executeNext(MCode) {
    for (const [i, Sign] of MCode.entries()) {
      console.log("time2:" + this.time2);
      await new Promise((resolve) => setTimeout(resolve, this.time2));
      if (i > 0) {
        console.log("inside delimiter gray color. i value is " + i);
        this.color = "medium";
        await new Promise((resolve) => setTimeout(resolve, 75));
      }
      if (Sign == "B") {
        console.log("Black color");
        this.color = "dark";
        this.time2 = 75;
        this.count++;
        this.data.length == this.count ? this.dismissModal() : null;
      } else if (Sign == "W") {
        console.log("white color");
        this.color = "light";
        this.time2 = 75;
        this.count++;
        this.data.length == this.count ? this.dismissModal() : null;
      } else if (Sign == "G") {
        console.log("gray color");
        this.color = "medium";
        this.time2 = 300;
        this.count++;
        this.data.length == this.count ? this.dismissModal() : null;
      }
    }
  }

  dismissModal() {
    console.log("Dissmiss Modal");
    setTimeout(() => {
      console.log("dissmiss modal but wait for 2 seconds");

      this.vibration.vibrate(1000);

      this.nativeAudio.play("uniqueId1").then(
        (res) => {
          console.log("res", res);
        },
        (err) => {
          console.log("err", err);
        }
      );

      let brightnessValue = 0.1;
          // this.brightness.setBrightness(brightnessValue);
      this.modal.dismiss("Okay");
    }, 2000);
  }
}

// export class FlashPage implements OnInit {
//   @Input() data: any;
//   color: any = "medium";

//   time = 25;

//   timeleft = 4;
//   downloadTimer: any;
//   show = true;

//   count = 0;

//   time2: number = 0;

//   LongDelay = 2000;
//   ShortDelay = 1000;

//   constructor(
//     public modal: ModalController,
//     public brightness: Brightness,
//     private cd: ChangeDetectorRef
//   ) {}

//   ngOnInit() {
//     var obj = this;
//     this.downloadTimer = setInterval(function () {
//       if (obj.timeleft <= 0) {
//         clearInterval(obj.downloadTimer);
//         obj.show = false;

//         obj.Execute(obj.data);
//         // obj.flashFunc();
//       } else {
//         document.getElementById("countdown").innerHTML =
//           obj.timeleft + " seconds remaining";
//       }
//       obj.timeleft -= 1;
//     }, 1000);
//   }

//   ionViewDidLeave() {
//     console.log("ionViewDidLeave");
//     clearInterval();
//   }

//   async flashFunc() {
//     console.log("data", this.data);
//     await this.data.forEach((brightnesslevel, i) => {
//       setTimeout(() => {
//         if (brightnesslevel == ".") {
//           console.log("Black color");
//           this.color = "dark";
//           this.count++;
//           this.data.length == this.count ? this.dismissModal() : null;
//         } else if (brightnesslevel == "_") {
//           console.log("white color");
//           this.color = "light";
//           this.count++;
//           this.data.length == this.count ? this.dismissModal() : null;
//         } else if (brightnesslevel == "G") {
//           console.log("gray color");
//           this.color = "medium";
//           this.count++;
//           this.data.length == this.count ? this.dismissModal() : null;
//         } else if (brightnesslevel == 5) {
//           this.time = 5;
//           console.log("time", this.time);
//           this.count++;
//           this.data.length == this.count ? this.dismissModal() : null;
//         } else {
//           this.time = 25;
//           console.log("time", this.time);
//           this.count++;
//           this.data.length == this.count ? this.dismissModal() : null;
//         }
//       }, i * this.time);
//     });
//     console.log("this.data.length", this.data.length);
//     // if (this.data.length == this.count) {
//     //   console.log("cotnt", this.count)

//     //   this.dismissModal()
//     // }
//     // console.log("this.data.length 2",this.data.length)
//   }

//   // async testpromise(element, length) {
//   //   return new Promise((resolve, reject) => {
//   //     console.log("middle");
//   //     if (element == "B") {
//   //       this.color = "dark";
//   //     }
//   //     resolve(element);
//   //   });
//   // }

//   async getFrutis(fruit, length) {
//     return new Promise((resolve) => {
//       console.log("inside promise");

//       this.count++;
//       console.log("count", this.count);
//       console.log("--------");
//       if (fruit == "B") {
//         this.color = "dark";
//         this.time2 = 25;
//         console.log("Black color: ", this.color);
//       } else if (fruit == "W") {
//         this.color = "light";
//         this.time2 = 25;
//         console.log("white color: ", this.color);
//       } else if (fruit == "G") {
//         this.color = "medium";
//         this.time2 = 300;
//         console.log("gray color: ", this.color);
//       }
//       console.log("--------");
//       setTimeout(resolve, this.time2);
//     });
//   }

//   getDelayFruits(fruit, length) {
//     return new Promise((resolve) => {
//       console.log("inside dealy promise");
//       this.color = "medium";
//       this.time2 = 25;
//       setTimeout(resolve, this.time2);
//     });
//   }

//   dissModallll(fruit, length) {
//     return new Promise((resolve) => {
//       console.log("inside dealy modal");
//       length == this.count ? this.dismissModal() : null;
//       setTimeout(resolve, 0);
//     });
//   }
//   async Execute(MCode) {
//     console.log("MCode", MCode);
//     //set brightness level
//     let brightnessValue = 1.0;
//     this.brightness.setBrightness(brightnessValue);
//     // MCode = ["B", "G", "W", "G"];
//     for (const fruit of MCode) {
//       const num = await this.getFrutis(fruit, MCode.length);
//       const num2 = await this.getDelayFruits(fruit, MCode.length);
//       const num3 = await this.dissModallll(fruit, MCode.length);
//       console.log("after promise " + num);
//     }

//   }

//     // const forEachllop = async _ => {
//     //   console.log("start");
//     //   console.log("-------------");
//     //  for (const )
//     //     const num = await this.getFrutis(fruit);
//     //     console.log("after promise " + num);

//     //   console.log("-------------");
//     //   console.log("end");
//     // };
//     ///////////////////////////////////////////////////////////////////////////////////////////////////////

//     // var myfunc03 = async function (i) {
//     //   console.log("number", i + "MCode[i]>>>", MCode[i]);

//     //   if (MCode[i] == "B") {
//     //     thi.color = "dark";
//     //     console.log("Black color short delay: ", thi.color);
//     //     thi.count++;
//     //     thi.time2 = 500;
//     //     MCode.length == thi.count ? thi.dismissModal() : null;
//     //   } else if (MCode[i] == "W") {
//     //     thi.color = "light";
//     //     console.log("white color short delay", thi.color);
//     //     thi.count++;
//     //     thi.time2 = 500;

//     //     MCode.length == thi.count ? thi.dismissModal() : null;
//     //   } else if (MCode[i] == "G") {
//     //     thi.color = "medium";
//     //     console.log("gray color long delay", thi.color);
//     //     thi.count++;
//     //     thi.time2 = 2000;

//     //     MCode.length == thi.count ? thi.dismissModal() : null;
//     //   }
//     //   // thi.color = "medium";
//     //   // console.log("gray color which is short delay", thi.color);
//     //   // // thi.count++;
//     //   // thi.time2 = 2000;

//     //   // MCode.length == thi.count ? thi.dismissModal() : null;
//     // };

//     // var thi = this;

//     // var myFunc01 = function () {
//     //   myfunc03(thi.count);
//     //   thi.count += 1;
//     //   setTimeout(function () {
//     //     if (thi.count <= MCode.length) {
//     //       myFunc01();
//     //     }
//     //   }, thi.time2);
//     // };

//     // myFunc01();

//     //////////////////////////////////////////////////////////////////////////////////////////////////////
//     // MCode.forEach((c, i) => {
//     //   function pause(milliseconds) {
//     //     let dt = new Date();
//     //     while (new Date().valueOf() - dt.valueOf() <= milliseconds) {
//     //     }
//     //   }

//     //   if (c == "B") {
//     //     pause(500);
//     //     this.color = "dark";
//     //     console.log("Black color: ", this.color);
//     //     this.count++;
//     //     MCode.length == this.count ? this.dismissModal() : null;
//     //   } else if (c == "W") {
//     //     pause(500);
//     //     this.color = "light";
//     //     console.log("white color", this.color);
//     //     this.count++;
//     //     MCode.length == this.count ? this.dismissModal() : null;
//     //   } else if (c == "G") {
//     //     pause(1000);
//     //     this.color = "medium";
//     //     console.log("gray color", this.color);
//     //     this.count++;
//     //     MCode.length == this.count ? this.dismissModal() : null;
//     //   }
//     // });

//     // for (let index = 0; index < MCode.length; index++) {
//     //   // const element = MCode[index];
//     //   console.log("index", MCode[index]);

//     //   setTimeout(() => {
//     //     if (MCode[index] == "B") {
//     //       this.color = "dark";
//     //       this.time2 = 500;
//     //       console.log("Black color" + "time::::" + this.time2);
//     //       this.count++;
//     //       MCode.length == this.count ? this.dismissModal() : null;
//     //     } else if (MCode[index] == "W") {
//     //       this.color = "light";
//     //       this.time2 = 500;
//     //       console.log("white color" + "time::::" + this.time2);
//     //       this.count++;
//     //       MCode.length == this.count ? this.dismissModal() : null;
//     //     } else if (MCode[index] == "G") {
//     //       this.color = "medium";
//     //       this.time2 = 3000;
//     //       console.log("gray color" + "time::::" + this.time2);
//     //       this.count++;
//     //       MCode.length == this.count ? this.dismissModal() : null;
//     //     }
//     //   }, index * this.time2);

//     //   console.log("time", this.time2);
//     // }

//     // for (const Sign of MCode) {
//     //   console.log("Sign",Sign)
//     //   setTimeout(() => {
//     //         if (Sign == "B") {
//     //           console.log("Black color");
//     //           this.color = "dark";
//     //           this.time2 = 1000;
//     //           this.count++;
//     //           this.data.length == this.count ? this.dismissModal() : null;
//     //         } else if (Sign == "W") {
//     //           console.log("white color");
//     //           this.color = "light";
//     //           this.time2 = 1000;
//     //           this.count++;
//     //           this.data.length == this.count ? this.dismissModal() : null;
//     //         } else if (Sign == "G") {
//     //           console.log("gray color");
//     //           this.color = "medium";
//     //           this.time2 = 2000;
//     //           this.count++;
//     //           this.data.length == this.count ? this.dismissModal() : null;
//     //         }
//     //       }, this.time2);
//     // }

//     // await MCode.forEach((Sign, i) => {
//     //   setTimeout(() => {
//     //     if (Sign == "B") {
//     //       console.log("Black color");
//     //       this.color = "dark";
//     //       this.time2 = 1000;
//     //       this.count++;
//     //       this.data.length == this.count ? this.dismissModal() : null;
//     //     } else if (Sign == "W") {
//     //       console.log("white color");
//     //       this.color = "light";
//     //       this.time2 = 1000;
//     //       this.count++;
//     //       this.data.length == this.count ? this.dismissModal() : null;
//     //     } else if (Sign == "G") {
//     //       console.log("gray color");
//     //       this.color = "medium";
//     //       this.time2 = 2000;
//     //       this.count++;
//     //       this.data.length == this.count ? this.dismissModal() : null;
//     //     }
//     //   }, i * this.time2);
//     // });

//   // myFunc(Sign) {

//   //    console.log("Sign.....",Sign)
//   //     if (Sign == "B") {
//   //       //Change Background to Black
//   //       //delay for ShortDelay

//   //       console.log("Before B Short Delay");
//   //       this.time2 = 1000;
//   //       this.color = "dark";
//   //       console.log("Color" + this.color);
//   //       this.count++;
//   //       // MCode.length == this.count ? this.dismissModal() : null;
//   //       console.log("Case: ", Sign + "time====" + this.time2);

//   //     } else if (Sign == "W") {
//   //       //Change Background to white
//   //       //Delay for ShortDelay
//   //       console.log("Before W Short Delay");
//   //       this.time2 = 1000;
//   //       this.color = "light";
//   //       console.log("Color" + this.color);
//   //       this.count++;
//   //       // MCode.length == this.count ? this.dismissModal() : null;
//   //       console.log("Case: ", Sign + "time====" + this.time2);

//   //     } else if (Sign == "G") {
//   //       //Change Background to Gray
//   //       //Long Delay
//   //       console.log("Before G Long Delay");
//   //      this.time2 = 2000;
//   //       this.color = "medium";
//   //       console.log("Color" + this.color);
//   //       this.count++;
//   //       // MCode.length == this.count ? this.dismissModal() : null;
//   //       console.log("Case: ", Sign + "time====" + this.time2);

//   //     }
//   //     console.log("Before Await" + this.time2 );

//   //     // MCode.forEach((Sign, i) => {
//   //     //   console.log("Sign: ", Sign);
//   //     //   // Sign == "B" || Sign == "W" ? this.time2 = this.ShortDelay : this.time2 = this.LongDelay;
//   //     //   // console.log("this.time2: ",this.time2);

//   //     // });

//   //   }

//   // // sleep(ms) {
//   // //     return new Promise(resolve => setTimeout(resolve, ms));
//   // //   }

//   dismissModal() {
//     console.log("Dissmiss Modal");
//     this.modal.dismiss();
//   }
// }

//    setTimeout(() => {
//   if (Sign == "B") {
//     //Change Background to Black
//     //delay for ShortDelay
//     this.time2 = this.ShortDelay;
//     this.color = "dark";
//     this.count++;
//     MCode.length == this.count ? this.dismissModal() : null;
//     console.log("Case: ", Sign + "time====" + this.time2);
//   } else if (Sign == "W") {
//     //Change Background to white
//     //Delay for ShortDelay
//     this.time2 = this.ShortDelay;
//     this.color = "light";
//     this.count++;
//     MCode.length == this.count ? this.dismissModal() : null;
//     console.log("Case: ", Sign + "time====" + this.time2);
//   } else if (Sign == "G") {
//     //Change Background to Gray
//     //Long Delay
//     this.time2 = this.LongDelay;
//     this.color = "medium";
//     this.count++;
//     MCode.length == this.count ? this.dismissModal() : null;
//     console.log("Case: ", Sign + "time====" + this.time2);
//   }
//   // else {
//   //   //Change Background to Gray
//   //   //Short Delay
//   //   this.time2 = this.ShortDelay;
//   //   this.color = "medium";
//   //   this.count++;
//   //   let brightnessValue = 0.5;
//   //   this.brightness.setBrightness(brightnessValue);
//   //   MCode.length == this.count ? this.dismissModal() : null;
//   // }
//   console.log("Case: ", Sign + " time====" + this.time2 +  "i======" +i  +  "iiiii * time"+ i*this.time2) ;
// }, i * this.time2);
