import { Component, OnInit } from "@angular/core";
import { Platform } from "@ionic/angular";
import { CommonService } from "../common.service";
// import { Socket } from 'ngx-socket-io';

@Component({
  selector: "app-tab2",
  templateUrl: "./charging.page.html",
  styleUrls: ["./charging.page.scss"],
})
export class ChargingPage implements OnInit {
  ws: any;
  availableDevices: any = [];
  deviceConsumption: any;
  interval: any;
  constructor(public service: CommonService, public platform: Platform) {}

  ngOnInit() {
    // this.service.plug();
    this.service.getList();

    // this.service.availableDevicessss = [
    //   {
    //     CMD: "CONSUME",
    //     PLUG: 1,
    //     DeviceID: "XXXXX",
    //     CON: 240,
    //     TCON: 500,
    //     AMOUNT: 80,
    //     DUE: "YYY",
    //     showButton: true,
    //   },
    // ];
  }
  ionViewDidEnter() {
    var obj = this;
    console.log(
      "tab 2 page here" + JSON.stringify(this.service.connectedDevices)
    );
    this.availableDevices = this.service.connectedDevices;
    console.log("availableDevices  tabs 2", this.availableDevices);
    this.availableDevices = this.availableDevices.LIST;
    console.log(
      "connected devices tabs 2" + JSON.stringify(this.availableDevices)
    );

    this.service.interval = this.startInterval();
  }

  stopCharging(dev) {
    this.service.stopCharging(dev);
  }

  startInterval() {
    // return setInterval(() => {
    if (this.service.deviceConsumptions != "PHONESTOP") {
      console.log("calling from interval");
      // if()

      console.log("devicce conumption", this.service.deviceConsumptions);
      if (this.service.deviceConsumptions != undefined) {
        var deviceID: any;
        this.deviceConsumption = this.service.deviceConsumptions;

        deviceID = this.service.deviceConsumptions.LIST[0].DeviceName;
        console.log(
          "bal" + " " + this.service.deviceConsumptions.LIST[0].BALANCE
        );
        // this.availableDevices=this.service.deviceConsumptions
        if (this.availableDevices) {
          this.availableDevices.forEach((element) => {
            console.log(
              "element.DeviceName" + element.DeviceName + " " + deviceID
            );
            if (element.DeviceName == deviceID) {
              element.CON = this.service.deviceConsumptions.LIST[0].CON;
              element.TCON = this.service.deviceConsumptions.LIST[0].TCON;
              element.AMOUNT = this.service.deviceConsumptions.LIST[0].AMOUNT;
              element.CMD = this.deviceConsumption.CMD;
              this.service.balance =
                this.service.deviceConsumptions.LIST[0].BALANCE;
              if (this.deviceConsumption.CMD == "CONSUME") {
                this.service.chargingStatus = "CHARGING";
              } else {
                this.service.chargingStatus = "STOPPED";
              }
              console.log(
                "charging status after if else" + this.service.chargingStatus
              );
              // this.service.chargingStatus=this.deviceConsumption.CMD
            }
            console.log("conssdasd" + JSON.stringify(this.availableDevices));
            console.log("available CMD" + this.deviceConsumption.CMD);
            console.log("balance" + this.service.balance);
          });
        }
      } else {
        this.availableDevices = JSON.parse(this.service.connectedDevices);
        this.availableDevices = this.availableDevices.LIST;
        console.log("con", this.service.deviceConsumptions);
      }
    }
    // }
    // }, 1000);

    // }
  }
}
