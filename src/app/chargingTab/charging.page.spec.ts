import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChargingPage } from './charging.page';

describe('ChargingPage', () => {
  let component: ChargingPage;
  let fixture: ComponentFixture<ChargingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChargingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
