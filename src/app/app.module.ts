import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
// import { SplashScreen } from "@ionic-native/splash-screen/ngx";
// import { SplashScreen } from '@awesome-cordova-plugins/splash-screen/ngx';
// import { StatusBar } from "@ionic-native/status-bar/ngx";
// import { StatusBar } from '@awesome-cordova-plugins/status-bar/ngx';

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
// import { ScreenBrightness } from '@capacitor-community/screen-brightness';
// import { Brightness } from "@ionic-native/brightness/ngx";
// import { Geolocation } from "@ionic-native/geolocation/ngx";
import { Geolocation } from '@awesome-cordova-plugins/geolocation/ngx';
// import { Device } from "@ionic-native/device/ngx";
import { HttpClientModule } from "@angular/common/http";
// import { Push } from "@ionic-native/push/ngx";
// import { LottieSplashScreen } from "@ionic-native/lottie-splash-screen/ngx";
// import { Network } from "@ionic-native/network/ngx";
import { Network } from '@awesome-cordova-plugins/network/ngx';
// import { NativeGeocoder } from "@ionic-native/native-geocoder/ngx";
import { NativeGeocoder } from '@awesome-cordova-plugins/native-geocoder/ngx';

// import { LaunchNavigator } from "@ionic-native/launch-navigator/ngx";
import { LaunchNavigator } from '@awesome-cordova-plugins/launch-navigator/ngx';
import { PopoverComponent } from "./popover/popover.component";
import { Popover2Component } from "./popover2/popover2.component";
// import { NativeAudio } from "@ionic-native/native-audio/ngx";
// import { Vibration } from "@ionic-native/vibration/ngx";
import { Vibration } from '@awesome-cordova-plugins/vibration/ngx';

import { FormsModule } from "@angular/forms";
import { SuccesAndFailComponent } from "./succes-and-fail/succes-and-fail.component";
import { BarcodeScanner } from '@awesome-cordova-plugins/barcode-scanner/ngx';
import { NativeAudio } from '@awesome-cordova-plugins/native-audio/ngx';
// import { Storage } from '@ionic/storage';
import { IonicStorageModule } from '@ionic/storage-angular';

@NgModule({
  declarations: [AppComponent, PopoverComponent, Popover2Component,SuccesAndFailComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    FormsModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    // SocketIoModule.forRoot(config),
    AppRoutingModule,
  ],
  providers: [
    // StatusBar,
    Network,
    // GooglePlus,
    Geolocation,
    NativeAudio,
    // Push,
    // Brightness,
    // ScreenBrightness,
    NativeGeocoder,
    // LottieSplashScreen,
    BarcodeScanner,
    Vibration,
    // Facebook,
    // Device,
    LaunchNavigator,
    // SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
