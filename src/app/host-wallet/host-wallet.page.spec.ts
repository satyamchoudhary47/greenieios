import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HostWalletPage } from './host-wallet.page';

describe('HostWalletPage', () => {
  let component: HostWalletPage;
  let fixture: ComponentFixture<HostWalletPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HostWalletPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HostWalletPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
