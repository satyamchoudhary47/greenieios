import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HostWalletPage } from './host-wallet.page';

const routes: Routes = [
  {
    path: '',
    component: HostWalletPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HostWalletPageRoutingModule {}
