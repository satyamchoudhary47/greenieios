import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HostWalletPageRoutingModule } from './host-wallet-routing.module';

import { HostWalletPage } from './host-wallet.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HostWalletPageRoutingModule
  ],
  declarations: [HostWalletPage]
})
export class HostWalletPageModule {}
