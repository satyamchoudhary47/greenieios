import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { CommonService } from '../common.service';

declare var RazorpayCheckout: any;


@Component({
  selector: 'app-host-wallet',
  templateUrl: './host-wallet.page.html',
  styleUrls: ['./host-wallet.page.scss'],
})
export class HostWalletPage implements OnInit {

  TodayDate = new Date()

  constructor(public service:CommonService,
    public alertController:AlertController) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    console.log("ionViewDidEnter called")
    // this.service.getUserBalance();

  }


  async withdraw(params){
    console.log("add monmey")
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Enter the amount',
      inputs: [
        {
          name: 'name1',
          type: 'number',
          placeholder: 'Enter Amount'
        },

      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          cssClass: 'btnWarning',
          handler: (alt) => {
            console.log('Confirm amount', alt.name1);


            if(params=='w'){
              this.DoPayment(alt.name1);

            }
            // this.service.addMoney(alt.name1)

            // setTimeout(() => {
            //   this.ionViewDidEnter();

            // }, 500)
          }
        }
      ]
    });

    await alert.present();
  }


  DoPayment(amount) {
    console.log("start payment", RazorpayCheckout)
    var obj = this
    var options = {
      description: 'Add Money to your Wallet',
      image: 'https://www.flaticon.com/svg/vstatic/svg/3445/3445580.svg?token=exp=1612260399~hmac=05a0608bc4e461cec13129312ad6ebe1',
      currency: 'INR',
      key: 'rzp_test_UEQcybnkm10wNp',
      amount: Number(amount * 100),
      name: 'Greenie',
      theme: {
        color: '#447F08'
      }
    }

    var successCallback = function (success) {

      alert('payment_id: ' + success.razorpay_payment_id)
      console.log("payment id", success.razorpay_payment_id)
      obj.service.addMoney(amount)

      setTimeout(() => {
        obj.ionViewDidEnter();

      }, 500)
      // var orderId = success.razorpay_order_id
      // var signature = success.razorpay_signature
    }
    var cancelCallback = function (error) {
    }
    RazorpayCheckout.on('payment.success', successCallback)
    RazorpayCheckout.on('payment.cancel', cancelCallback)
    RazorpayCheckout.open(options)

  }

}
