import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { TabsPage } from "./tabs.page";

const routes: Routes = [
  {
    path: "tabs",
    component: TabsPage,
    children: [
      {
        path: "home",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../home/home.module").then((m) => m.HomePageModule),
          },
        ],
      },
      {
        path: "charging",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../chargingTab/charging.module").then(
                (m) => m.ChargingPageModule
              ),
          },
        ],
      },
      {
        path: "wallet",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../walletTab/wallet.module").then(
                (m) => m.walletPageModule
              ),
          },
        ],
      },
      {
        path: "device",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../deviceTab/device.module").then(
                (m) => m.devicePageModule
              ),
          },
        ],
      },
      {
        path: "user",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../user/user.module").then(
                (m) => m.UserPageModule
              ),
          },
        ],
      },
      {
        path: "",
        redirectTo: "/tabs/home",
        pathMatch: "full",
      },
    ],
  },
  {
    path: "",
    redirectTo: "/tabs/home",
    pathMatch: "full",
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
