import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
// import { Network } from "@ionic-native/network/ngx";
import { Network } from '@awesome-cordova-plugins/network/ngx';
import { ModalController, Platform } from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { CommonService } from "../common.service";
import { FlashPage } from "../flash/flash.page";

@Component({
  selector: "app-tabs",
  templateUrl: "./tabs.page.html",
  styleUrls: ["./tabs.page.scss"],
})
export class TabsPage implements OnInit {
  ws: any;

  codeFromBackend: any = "";
  SHORT_DELAY = 5;
  LONG_DELAY = 25;

  constructor(
    public platform: Platform,
    public modalController: ModalController,
    public router: Router,
    public storage: Storage,
    public network: Network,
    public service: CommonService
  ) {}
  ionViewDidEnter() {}

  ngOnInit() {
    // this.service.getUserOwnedDevices();
  }

  checkForNetwork() {

    if(this.service.networkIsConnected){
      this.service.scan();
    }
    else{
      this.continueWithMorsc();
    }
  }
  async scan() {
    // this.service.scan();
    this.checkForNetwork();
  }

  continueWithMorsc() {
    console.log("internet exits")
    this.storage.get("loginUserIdForMors").then((res) => {
      console.log("code to convert into mors", res);
      console.log("from local service", this.service.loginUserIdForMors);

      if (this.service.loginUserIdForMors) {
        this.codeFromBackend = this.service.loginUserIdForMors;
      } else {
        this.codeFromBackend = res;
      }

      console.log("codeFromBackend", this.codeFromBackend.CODE);

      // console.log(this.getCode(this.codeFromBackend.CODE));

      var mors: any = this.getMorse(this.codeFromBackend.CODE);
      // var mors = this.getCode(this.codeFromBackend.CODE);

      mors = [...mors];

      if (mors) {
        this.openModal(mors);
      } else {
        this.service.showToast("Morsc Code is not Present");
      }
    });
  }

  getMorse(Code) {
    Code = Code.toUpperCase();
    var ConvertionArray = [
      "BW",
      "WBBB",
      "WBWB",
      "WBB",
      "B",
      "BBWB",
      "WWB",
      "BBBB",
      "BB",
      "BWWW",
      "WBW",
      "BWBB",
      "WW",
      "WB",
      "WWW",
      "BWWB",
      "WWBW",
      "BWB",
      "BBB",
      "W",
      "BBW",
      "BBBW",
      "BWW",
      "WBBW",
      "WBWW",
      "WWBB",
      "BWWWW",
      "BBWWW",
      "BBBWW",
      "BBBBW",
      "BBBBB",
      "WBBBB",
      "WWBBBB",
      "WWWBB",
      "WWWWB",
      "WWWWW",
    ];

    var ConvertedString = "";
    for (var i = 0; i <= Code.length - 1; i++) {
      var value = 0;
      // debugger;
      console.log("Code char" + Code.charCodeAt(i));
      if (Code.charCodeAt(i) == 48) value = 35;

      if (Code.charCodeAt(i) >= 49 && Code.charCodeAt(i) <= 57)
        value = Code.charCodeAt(i) - 23;

      if (Code.charCodeAt(i) >= 65 && Code.charCodeAt(i) <= 90)
        value = Code.charCodeAt(i) - 65;

      ConvertedString += ConvertionArray[value] + "G";
    }

    console.log("ConvertedString", ConvertedString);
    return ConvertedString;
  }

  async openModal(code) {
    console.log("log of modal", code);
    const modal = await this.modalController.create({
      component: FlashPage,
      backdropDismiss: false,
      cssClass: "my-custom-modal-css",
      componentProps: { data: code },
    });
    await modal.present();

    await modal.onWillDismiss().then((res) => {
      console.log("modal on disssmiss", res);
      if (res.data == "Okay") {
        this.router.navigateByUrl("/tabs/charging");
      }
    });
  }

  getCode(Code: string) {
    var ArrayOfMorse = [];
    // Code = Code.toUpperCase();
    var pointCounter = 0;
    for (var i = 0; i <= Code.length - 1; i++) {
      var CodeX = 0;
      if (Code.charAt(i) >= "A" && Code.charAt(i) <= "Z")
        CodeX = Code.charCodeAt(i) - 64;
      else CodeX = Code.charCodeAt(i) - 48 + 26;

      switch (CodeX) {
        case 1:
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          break;
        case 2:
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          break;
        case 3:
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          break;
        case 4:
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          break;
        case 5:
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          break;
        case 6:
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          break;
        case 7:
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          break;
        case 8: //H
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          break;
        case 9:
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          break;
        case 10: //J
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          break;
        case 11: //K
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          break;
        case 12: //L
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          break;
        case 13: //M
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          break;
        case 14: //N
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          break;
        case 15: //O
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          break;
        case 16: //P
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          break;
        case 17: //Q
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          break;
        case 18: //R
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          break;
        case 19: //S
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          break;
        case 20: //T
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          break;
        case 21: //U
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          break;
        case 22: //V
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          break;
        case 23: //W
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          break;
        case 24: //X
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          break;
        case 25: //Y
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          break;
        case 26: //Z
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          break;
        case 27: //1
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          break;
        case 28: //2
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          break;
        case 29: //3
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          break;
        case 30: //4
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          break;
        case 31: //5
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          break;
        case 32: //6
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          break;
        case 33: //7
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          break;
        case 34: //8
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          break;
        case 35: //9
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, ".");
          break;
        case 36: //0
          pointCounter = this.Start(ArrayOfMorse, pointCounter);
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          pointCounter = this.AddToPoint(ArrayOfMorse, pointCounter, "_");
          break;
      }
    }
    return ArrayOfMorse;
  }

  Start(ArrayOfMorse, pointCounter) {
    ArrayOfMorse[pointCounter++] = "G";
    ArrayOfMorse[pointCounter++] = this.LONG_DELAY;
    return pointCounter;
  }

  AddToPoint(ArrayOfMorse, pointCounter, ToAdd) {
    ArrayOfMorse[pointCounter++] = "G";
    ArrayOfMorse[pointCounter++] = this.SHORT_DELAY;
    ArrayOfMorse[pointCounter++] = ToAdd;
    ArrayOfMorse[pointCounter++] = this.SHORT_DELAY;
    return pointCounter;
  }
}
