import { async } from "@angular/core/testing";
import { Component } from "@angular/core";
import {
  AlertController,
  LoadingController,
  MenuController,
  ModalController,
  Platform,
  ToastController,
} from "@ionic/angular";
// import { Geolocation } from "@ionic-native/geolocation/ngx";
import { Geolocation } from '@awesome-cordova-plugins/geolocation/ngx';
// import { Device } from "@ionic-native/device/ngx";
import { CommonService } from "../common.service";
import { DeviceOnMapPage } from "../device-on-map/device-on-map.page";
import { SuccesAndFailComponent } from '../succes-and-fail/succes-and-fail.component';


declare var google;

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage {
  map: any;
  marker: any;
  latitude: any = "";
  longitude: any = "";
  timestamp: any = "";
  ws: any;

  constructor(
    public loadingController: LoadingController,
    private geolocation: Geolocation,
    // public device: Device,
    public platform: Platform,
    public modalController: ModalController,
    public menu: MenuController,
    public service: CommonService,
    public toast: ToastController,
    public alertController: AlertController
  ) {
    menu.enable(true);
  }

  ngOnInit() {
    console.log("On Home");

    // this.getDevicesAndLoadMap();
    // this.service.getDevices();s
    setTimeout(() => {
      this.loadMap();
    }, 1500);
  }

  async getDevicesAndLoadMap() {
    await this.service.getDevices().then((res) => {
      console.log("getDevices:: ", res);

      this.loadMap();
    });
  }
  ionViewDidEnter() {
    // this.loadMap();

    this.geolocation.getCurrentPosition().then(
      (position) => {
        console.log("Position", position);
      },
      (error) => {
        console.log("error", error);
      }
    );
  }
  loadMap() {
    console.log("location for map", this.service.deviceListForHomePage);

    var thi = this;

    var gps = new google.maps.LatLng(19.03142625277601, 72.88110477352579);
    var map = new google.maps.Map(document.getElementById("map"), {
      position: gps,
      mapTypeControl: false,
      zoom: 9,
      center: new google.maps.LatLng(19.2, 72.86),
      styles: [
        {
          elementType: "geometry",
          stylers: [{ color: "#f5f5f5" }],
        },
        {
          elementType: "labels.icon",
          stylers: [{ visibility: "off" }],
        },
        {
          elementType: "labels.text.fill",
          stylers: [{ color: "#616161" }],
        },
        {
          elementType: "labels.text.stroke",
          stylers: [{ color: "#f5f5f5" }],
        },
        {
          featureType: "administrative.land_parcel",
          elementType: "labels.text.fill",
          stylers: [{ color: "#bdbdbd" }],
        },
        {
          featureType: "poi",
          elementType: "geometry",
          stylers: [{ color: "#eeeeee" }],
        },
        {
          featureType: "poi",
          elementType: "labels.text.fill",
          stylers: [{ color: "#757575" }],
        },
        {
          featureType: "poi.park",
          elementType: "geometry",
          stylers: [{ color: "#e5e5e5" }],
        },
        {
          featureType: "poi.park",
          elementType: "labels.text.fill",
          stylers: [{ color: "#9e9e9e" }],
        },
        {
          featureType: "road",
          elementType: "geometry",
          stylers: [{ color: "#ffffff" }],
        },
        {
          featureType: "road.arterial",
          elementType: "labels.text.fill",
          stylers: [{ color: "#757575" }],
        },
        {
          featureType: "road.highway",
          elementType: "geometry",
          stylers: [{ color: "#dadada" }],
        },
        {
          featureType: "road.highway",
          elementType: "labels.text.fill",
          stylers: [{ color: "#616161" }],
        },
        {
          featureType: "road.local",
          elementType: "labels.text.fill",
          stylers: [{ color: "#9e9e9e" }],
        },
        {
          featureType: "transit.line",
          elementType: "geometry",
          stylers: [{ color: "#e5e5e5" }],
        },
        {
          featureType: "transit.station",
          elementType: "geometry",
          stylers: [{ color: "#eeeeee" }],
        },
        {
          featureType: "water",
          elementType: "geometry",
          stylers: [{ color: "#c9c9c9" }],
        },
        {
          featureType: "water",
          elementType: "labels.text.fill",
          stylers: [{ color: "#9e9e9e" }],
        },
      ],
    });

    var infowindow = new google.maps.InfoWindow();
    var labels = "ABC ";
    var labelIndex = 0;
    var marker, i;
    for (i = 0; i < this.service.deviceListForHomePage.length; i++) {
      marker = new google.maps.Marker({
        icon: {
          url: "assets/marker.svg",
          scaledSize: new google.maps.Size(30, 30),
        },
        position: new google.maps.LatLng(
          this.service.deviceListForHomePage[i].LAT,
          this.service.deviceListForHomePage[i].LON
        ),
        // label: labels[labelIndex++ % labels.length],
        map: map,
      });
      google.maps.event.addListener(
        marker,
        "click",
        (function (marker, i) {
          return function () {
            thi.openModal(thi.service.deviceListForHomePage[i]);
          };
        })(marker, i)
      );
    }
  }


async openModal(data) {
  console.log("modal data", data);
  const modal = await this.modalController.create({
    // component: SuccesAndFailComponent,
    component: DeviceOnMapPage,
    backdropDismiss: true,
    swipeToClose: true,
   cssClass: "my-custom-modal-cssforDevice",
    // cssClass: "my-custom-modal-cssforTransaction",
    componentProps: { data: data },
  });
  await modal.present();
}

}

export class Marker {
  position: {
    lat: number;
    lng: number;
  };
  title: string;
}

// console.log(
//   "r"
// )
// var mapOptions = {
//   center: { lat: 19.1849348, lng: 72.8397202 },
//   zoom: 15,
//   controls: {
//     compass: true,
//     myLocationButton: true,
//     indoorPicker: true,
//     myLocation: true,
//     zoom: true
//   },
//   gestures: {
//     scroll: true,
//     tilt: true,
//     rotate: true,
//     zoom: true
//   }
// }

// this.map = new google.maps.Map(document.getElementById('map'), mapOptions);

// this.geolocation.getCurrentPosition().then((resp) => {
//   console.log("reee", resp)

//   this.latitude = resp.coords.latitude;
//   this.longitude = resp.coords.longitude;
//   console.log("latitude", this.latitude)
//   console.log("longitude", this.longitude)
//   console.log("resp.coords.accuracy", resp.coords.accuracy)

//   var gps = new google.maps.LatLng(this.latitude, this.longitude);

//   if (this.marker == null) {

//     this.marker = new google.maps.Marker({
//       position: gps,
//       map: this.map,
//       myLocation: true,
//       animation: google.maps.Animation.DROP,

//       controls: { myLocation: true },
//       title: "My Position",
//       icon: {
//         url: 'assets/blueIcon.png',
//         scaledSize: new google.maps.Size(50, 50)
//       }
//     })
//   }
//   else {
//     console.log('else')
//     this.marker = new google.maps.Marker({
//       position: gps,
//       map: this.map,
//       myLocation: true,
//       animation: google.maps.Animation.DROP,

//       controls: { myLocation: true },
//       title: "My Position",
//       icon: {
//         url: 'assets/blueIcon.png',
//         scaledSize: new google.maps.Size(50, 50)
//       }
//     })
//   }
//   this.map.panTo(gps);
// }).catch((error) => {
//   alert('Error getting location' + JSON.stringify(error));
// });

// const geocoder = new google.maps.Geocoder();

// const latlng = {
//   lat: 19.209279442895827,
//   lng: 72.97103146719772,
// };

// geocoder.geocode({ location: latlng }).then((response) => {
//   if (response.results[0]) {
//     console.log("response", response);
//   }
// });

// var locations = [
//   // ["Carnival Cinemas", 19.03142625277601, 72.88110477352579],
//   ["Cinépolis Viviana", 19.209279442895827, 72.97103146719772],
//   ["Cronulla Beach", 19.173380265269365, 72.83751682078703],
// ];
// var gps = new google.maps.LatLng(19.03142625277601, 72.88110477352579);
// var map = new google.maps.Map(document.getElementById("map"), {
//   position: gps,
//   zoom: 9,
//   center: new google.maps.LatLng(19.2, 72.86),
// });
// var infowindow = new google.maps.InfoWindow();
// var labels = "ABC ";
// var labelIndex = 0;
// var marker, i;
// for (i = 0; i < locations.length; i++) {
//   marker = new google.maps.Marker({
//     position: new google.maps.LatLng(locations[i][1], locations[i][2]),
//     label: labels[labelIndex++ % labels.length],
//     map: map,
//   });
//   google.maps.event.addListener(
//     marker,
//     "click",
//     (function (marker, i) {
//       return function () {
//         infowindow.setContent(locations[i][0]);
//         infowindow.open(map, marker);
//         this.userDestination = [locations[i][1], locations[i][2]];
//         console.log(this.userDestination);
//       };
//     })(marker, i)
//   );
// }

// console.log(
//   "this.service.deviceListForHomePage[i].location",
//   thi.service.deviceListForHomePage[i].location
// );
// infowindow.setContent(
//   thi.service.deviceListForHomePage[i].location
// );
// infowindow.open(map, marker);
// this.userDestination = [
//   thi.service.deviceListForHomePage[i].LAT,
//   thi.service.deviceListForHomePage[i].LON,
// ];
// console.log(this.userDestination);
