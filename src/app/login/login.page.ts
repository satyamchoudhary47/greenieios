import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { GooglePlus } from "@ionic-native/google-plus/ngx";
import {
  AlertController,
  LoadingController,
  MenuController,
  NavController,
  Platform,
  ToastController,
} from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { CommonService } from '../common.service';
// import { Facebook, FacebookLoginResponse } from "@ionic-native/facebook/ngx";
import { timeStamp } from 'console';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loading: any;

  userData: any;

  show = false;
  number: any;
  otp: string;
  showLoginBlock = true;

  constructor(
    // private google: GooglePlus,
    public loadingController: LoadingController,
    private platform: Platform,
    public toast: ToastController,
    public menu: MenuController,
    // public fb: Facebook,
    private router: Router,
    private storage: Storage,
    private navCtrl: NavController,
    private service: CommonService,
    public alertController: AlertController
  ) {
    menu.enable(false);
  }

  sendOtp() {
    console.log('is socket connected', this.service.isSocketConnected);
    console.log('user Phone Number', this.number + typeof this.number);
    let thi = this;

    if (this.service.isSocketConnected == false) {
      console.log('socket is closed');

      // this.service.connectToSocket(false);
      this.service.ws = new WebSocket('ws://13.126.140.93:4920');
      this.service.ws.onopen = function (e) {
        if (thi.number.toString().length == 10) {
          console.log('device id', thi.service.deviceId);
          thi.service.mobileNumber = thi.number;
          thi.show = true;
          thi.showLoginBlock = false;
          thi.service.regDevice(false);
        } else {
          thi.service.showToast('Please Enter the valid Mobile Number');
        }
      };
    } else {
      if (this.number.toString().length == 10) {
        console.log('device id', this.service.deviceId);
        this.service.mobileNumber = this.number;
        this.show = true;
        this.showLoginBlock = false;
        this.service.regDevice(false);
      } else {
        this.service.showToast('Please Enter the valid Mobile Number');
      }
    }
  }

  dintgetOtp() {
    this.show = false;
    this.showLoginBlock = true;
  }
  async login() {
    // this.otp = this.service.otp

    console.log('this.otp', this.otp);

    this.service.verifyOtp(this.otp).then((res: any) => {
      console.log('res', res);
      let ress = JSON.parse(res);
      console.log('res', ress);
      if (ress.ACTION == 'OK') {
        console.log('action ok');

        setTimeout(() => {
          this.navCtrl.navigateRoot('root');
          this.router.navigateByUrl('/tabs');
        }, 100);
      } else if (ress.ACTION == 'FAIL') {
        this.service.showToast('Please enter valid OTP');
      }
    });
  }

  async googleLogin() {
    // this.router.navigateByUrl('/tabs')
    // this.service.presentLoading("Please Wait...");
    // this.google.getSigningCertificateFingerprint().then((res) => {
    //   console.log("signcertificate", res);
    // });
    // this.google
    //   .login({
    //     webClientId:
    //       "636472206040-sv76on04ipt0n8ijlu6kf40lcd79rrus.apps.googleusercontent.com",
    //     offline: false,
    //     scope: "email,name,photo",
    //   })
    //   .then((result) => {
    //     this.service.dismissLoading();
    //     this.service.LoginType = "google";
    //     console.log("Googledata" + JSON.stringify(result));
    //     this.storage.set("currentUser", result);
    //     // alert(JSON.stringify(result))
    //     this.service.currentUser = result;
    //     console.log(this.service.currentUser);
    //     if (result) {
    //       this.service.startSocket();
    //       setTimeout(() => {
    //         this.navCtrl.navigateRoot("root");
    //         this.router.navigateByUrl("/tabs");
    //       }, 100);
    //     }
    //   })
    //   .catch((err) => {
    //     this.service.dismissLoading();
    //     alert(err);
    //     console.log(err);
    //   });
  }

  async ngOnInit() {
    this.loading = await this.loadingController.create({
      message: 'Connecting ...',
    });
  }
  async presentLoading(loading) {
    await loading.present();
  }

  facebook() {
    this.service.currentUser.email = 'satyamchoudhary477@gmail.com';
    this.service.startSocket();
    setTimeout(() => {
      this.navCtrl.navigateRoot('root');
      this.router.navigateByUrl('/tabs');
    }, 100);

    // this.fb.logout().then((res) => {
    //   console.log("logout resposne", res);
    // });

    // this.fb
    //   .login(["email", "public_profile"])
    //   .then((response: FacebookLoginResponse) => {

    //     console.log("response", response);

    //     this.getUserDetail(response.authResponse.userID);
    //   });
  }

  getUserDetail(userid: any) {
    // this.fb
    //   .api("/" + userid + "/?fields=id,email,name,picture", ["public_profile"])
    //   .then((res) => {
    //     console.log(res);
    //     alert(res.id);
    //     if (res.id) {
    //       this.service.LoginType = "fb";
    //       // this.service.currentUser.email = res.id;
    //       // this.service.currentUser.imageUrl = res.picture.data.url
    //       // this.service.currentUser.displayName = res.name
    //       let fbres = {
    //         email: res.email,
    //         imageUrl: res.picture.data.url,
    //         displayName: res.name,
    //         id: res.id,
    //         fb: true,
    //       };
    //       this.service.currentUser = fbres;
    //       this.storage.set("currentUser", fbres);
    //       // alert(JSON.stringify(result))
    //       console.log(this.service.currentUser);
    //       this.service.startSocket();
    //       setTimeout(() => {
    //         this.navCtrl.navigateRoot("root");
    //         this.router.navigateByUrl("/tabs");
    //       }, 100);
    //     }
    //   })
    //   .catch((e) => {
    //     console.log(e);
    //   });
  }
}
