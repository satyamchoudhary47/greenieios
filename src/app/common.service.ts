import { async } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
// import { GooglePlus } from "@ionic-native/google-plus/ngx";
import {
  AlertController,
  LoadingController,
  ModalController,
  NavController,
  Platform,
  ToastController,
} from '@ionic/angular';
import { Storage } from '@ionic/storage';

// import {
//   NativeGeocoder,
//   NativeGeocoderResult,
//   NativeGeocoderOptions,
// } from "@ionic-native/native-geocoder/ngx";
import {
  NativeGeocoder,
  NativeGeocoderResult,
  NativeGeocoderOptions,
} from '@awesome-cordova-plugins/native-geocoder/ngx';

import { element } from 'protractor';
import { Location } from '@angular/common';
// import * as moment from "moment";
import { SuccesAndFailComponent } from './succes-and-fail/succes-and-fail.component';
// import { BarcodeScanner } from "@ionic-native/barcode-scanner/ngx";
import { BarcodeScanner } from '@awesome-cordova-plugins/barcode-scanner/ngx';
import { EmailValidator } from '@angular/forms';
import { GetEmailPage } from './get-email/get-email.page';
// import { Facebook } from "@ionic-native/facebook/ngx";

@Injectable({
  providedIn: 'root',
})
export class CommonService {
  networkIsConnected: boolean = true;
  wss: WebSocket;
  qrcodeData: any = '';
  ws: any;
  deviceId: any = 'xqyzzzz';
  connectedDevices: any = [];
  devicesOwned: any = [];
  deviceLoaction: any;
  userHistory: any = [];
  walletHistory: any;
  userBalance: any;

  LoginType = 'google';

  isUser = false;

  otp: any;

  wifiList: any[];
  totalPrice = 0;

  deviceReportArray: any[];
  totalConsumption = 0;

  emailFromServer: any;
  isSocketConnected = false;
  deviceListForHomePage = [];
  deviceConsumptions: any = {
    CMD: null,
    LIST: [
      {
        DeviceName: null,
        CON: null,
        TCON: null,
        AMOUNT: 0,
        PLUG: null,
        ST: null,
        showButton: true,
        STATUS: null,
        BALANCE: null,
      },
    ],
  };

  availableDevicessss = [];
  balance: any = 0;
  chargingStatus: any = 'STOPPED';
  interval: any;
  isLoading = false;
  cartQuantity: Number = 0;
  scanQrCode: any;
  currentUser: any = {};

  loginUserIdForMors: any = '';
  emailValue: any;
  mobileNumber: any;

  loginResponse: any;

  constructor(
    public toastController: ToastController,
    public router: Router,
    public alertCtrl: AlertController,
    public nativeGeocoder: NativeGeocoder,
    public http: HttpClient,
    public location: Location,
    private storage: Storage,
    public barcodeScanner: BarcodeScanner,
    // public fb: Facebook,
    public navCtrl: NavController,
    public modalController: ModalController,
    public platform: Platform,
    // private GooglePlus: GooglePlus,
    public loadingCtrl: LoadingController
  ) {}

  ngOnInit() {}

  async connectToSocketold(shouldProceed) {
    console.log('connect To socket');

    setTimeout(() => {
      console.log(
        'connect To socket and check whether network is there or not'
      );

      if (this.networkIsConnected == false && shouldProceed == true) {
        console.log('navigate to root homepage offline mode');
        this.navCtrl.navigateRoot('/tabs');
      } else {
        this.ws = new WebSocket('ws://13.126.140.93:4920');

        // this.ws = new WebSocket("ws://localhost:4920");
        this.isSocketConnected = true;

        this.platform.ready().then(() => {
          var thi = this;

          this.ws.onopen = function () {
            var obj: any = {
              CMD: 'VER',
            };
            console.log('data ', JSON.stringify(obj));
            this.send(JSON.stringify(obj));
          };
          this.ws.onmessage = function (event) {
            console.log('response from version service' + '  ' + event.data);
            // if (shouldProceed) {
            //   thi.regDevice(true);
            // }
          };
          setTimeout(() => {
            thi.regDevice(true);
          }, 1000);

          this.ws.onerror = function (event) {
            console.log('error occured', event);
            thi.isSocketConnected = false;
          };
          this.ws.onclose = function () {
            console.log('socket connection closed');
            thi.isSocketConnected = false;

            // if(thi.networkIsConnected){
            //   thi.connectToSocket(true);
            // }
          };
        });
      }
    }, 1000);
  }

  async connectToSocket(shouldProceed) {
    console.log('connect To socket');

    setTimeout(() => {
      console.log(
        'connect To socket and check whether network is there or not'
      );

      if (this.networkIsConnected == false && shouldProceed == true) {
        console.log('navigate to root homepage');
        this.navCtrl.navigateRoot('/tabs');
      } else {
 
        
         this.ws =  new WebSocket('ws://13.126.140.93:4920'); //connection established
         this.isSocketConnected = true;
        

        console.log("socket should be connected")

        // this.ws = new WebSocket("ws://localhost:4920");
        this.platform.ready().then(() => {
          var thi = this;

          // this.ws.onopen = function () {
          //   var obj: any = {
          //     // CMD: "VER",
          //   };
          //   console.log("data ", JSON.stringify(obj));
          //   this.send(JSON.stringify(obj));
          // };
          // this.ws.onmessage = function (event) {
          //   console.log("response from version service" + "  " + event.data);

          // };
          setTimeout(() => {
            if (shouldProceed == true) {
              thi.regDevice(true);
            }
          }, 500);

          this.ws.onerror = function (event) {
            thi.isSocketConnected = false;


            console.log('error occured', event);
          };
          this.ws.onclose = function () {
            thi.isSocketConnected = false;

            console.log('socket connection closed', thi.isSocketConnected);
            // if(thi.networkIsConnected){
            //   thi.connectToSocket(true);
            // }
          };
        });
      }
    }, 1000);
  }

  regDevice(fromStorage) {
    console.log('reg device service called !!!');

    var thi = this;

    var obj1: any = {
      CMD: 'REG',
      DeviceID: thi.deviceId,
      NUMBER: thi.mobileNumber,
      Local: fromStorage, // true if it is from storage than means user already exits
    };

    console.log('device req', obj1);
    thi.ws.send(JSON.stringify(obj1));

    this.ws.onmessage = function (event) {
      console.log('response from server tabs' + '  ' + event.data);
      var res = JSON.parse(event.data);
      console.log('res', parseInt(res.OTP));
      if (res.CMD == 'OTP' && res.OTP) {
        thi.otp = parseInt(res.OTP);
      }

      var connectedDevices = JSON.parse(event.data);

      console.log('connectedDevices', connectedDevices);

      if (connectedDevices.CMD == 'OTP' && connectedDevices.OTP) {
        thi.otp = connectedDevices.OTP;
        // alert("otp is >>> " + thi.otp);
      }

      if (connectedDevices.CMD == 'REG') {
        thi.loginUserIdForMors = JSON.parse(event.data);

        console.log('connectedDevices.EMAIL', connectedDevices.EMAIL);
        if (connectedDevices.EMAIL) {
          thi.emailFromServer = connectedDevices.EMAIL;
          console.log('emailFromServer>>>>>>>', thi.emailFromServer);
        }

        console.log('loginUserIdForMors', thi.loginUserIdForMors);

        thi.storage.set('loginUserIdForMors', JSON.parse(event.data));

        console.log('loginUserIdForMors', thi.loginUserIdForMors.CODE);

        if (connectedDevices.CODE) {
          console.log('morsc code exits');

          thi.getDevices();
        }
        // thi.sendNewOtp();
      }

      console.log(
        'connected  devices tabs page' + JSON.stringify(connectedDevices)
      );
      console.log(
        'deviceConsumptions  devices tabs page' +
          JSON.stringify(this.deviceConsumptions)
      );

      if (connectedDevices.CMD == 'DEVICELIST') {
        console.log('reached');
        thi.connectedDevices = connectedDevices;
        console.log('DEVICE LIST' + JSON.stringify(this.connectedDevices));
      } else if (connectedDevices.CMD == 'CONSUME') {
        // thi.service.deviceConsumptions = connectedDevices

        thi.deviceConsumptions.CMD = connectedDevices.CMD;
        thi.deviceConsumptions.LIST[0].DeviceName = connectedDevices.DeviceID;
        thi.deviceConsumptions.LIST[0].CON = connectedDevices.CON;
        thi.deviceConsumptions.LIST[0].TCON = connectedDevices.TCON;
        thi.deviceConsumptions.LIST[0].AMOUNT = connectedDevices.AMOUNT;
        thi.deviceConsumptions.LIST[0].PLUG = connectedDevices.PLUG;
        thi.deviceConsumptions.LIST[0].BALANCE = connectedDevices.BALANCE;
        // thi.deviceConsumptions.LIST[0].AMOUNT = connectedDevices.AMOUNT;
        thi.balance = thi.deviceConsumptions.LIST[0].BALANCE;

        console.log('CONSUME LIST' + JSON.stringify(thi.deviceConsumptions));
      }
    };
  }

  // async startSocketttt() {
  //   console.log('Start Socket');

  //   // if(thi.LoginType == 'fb'){
  //   //   // thi.currentUser.email = this.currentUser.id
  //   //   this.emailValue = this.currentUser.id
  //   // }
  //   // else{
  //   //   this.emailValue = this.currentUser.email
  //   // }
  //   this.ws = new WebSocket('ws://13.126.140.93:4920');
  //   this.isSocketConnected = true;

  //   // this.ws = new WebSocket("ws://03.108.252.21:1010");
  //   // this.ws = new WebSocket("ws://13.234.110.18:1010");
  //   this.platform.ready().then(() => {
  //     var thi = this;
  //     //  this.socket.connect();

  //     // console.log("called show owner info", thi.currentUser.email);

  //     this.ws.onopen = function () {
  //       var obj: any = {
  //         CMD: 'REG',
  //         // DeviceID: "satyamchoudhary477@gmail.com",
  //         // DeviceID: thi.currentUser.email
  //         // ? thi.currentUser.email
  //         // : "satyamchoudhary477@gmail.com",
  //         // DeviceID: thi.emailValue,
  //         DeviceID: thi.deviceId,

  //         // DeviceID: "abc",
  //         NUMBER: JSON.stringify(thi.mobileNumber),
  //         Local: true,

  //         // DeviceID: thi.currentUser.email
  //         // ? thi.currentUser.email
  //         // : "greeniemax@gmail.com",
  //       };

  //       console.log('data ', JSON.stringify(obj));
  //       this.send(JSON.stringify(obj));

  //       // thi.getDevices();
  //       // thi.getList

  //       // setTimeout(() => {
  //       //   var obj1: any = {
  //       //     "CMD": "LOC", "LONG": "79.05241989999999", "LAT": "21.1051488", "RANGE": "100"
  //       //   }
  //       //   console.log("CMD LOC" + JSON.stringify(obj1))

  //       //   this.send(JSON.stringify(obj1))
  //       // }, 10)

  //       // setTimeout(() => {
  //       //   var obj2: any = {
  //       //     CMD: "LIST",
  //       //   };
  //       //   console.log("qr data" + JSON.stringify(obj2));
  //       //   this.send(JSON.stringify(obj2));
  //       // }, 10);
  //     };
  //     this.ws.onmessage = function (event) {
  //       console.log('response from server tabs' + '  ' + event.data);

  //       console.log('response from server tabs page data' + '  ' + event.data);

  //       var connectedDevices = JSON.parse(event.data);

  //       console.log('connectedDevices', connectedDevices);

  //       if (connectedDevices.CMD == 'OTP' && connectedDevices.OTP) {
  //         thi.otp = connectedDevices.OTP;
  //         // alert("otp is >>> " + thi.otp);
  //       }

  //       if (connectedDevices.CMD == 'REG') {
  //         thi.loginUserIdForMors = JSON.parse(event.data);

  //         console.log('connectedDevices.EMAIL', connectedDevices.EMAIL);
  //         if (connectedDevices.EMAIL) {
  //           thi.emailFromServer = connectedDevices.EMAIL;
  //           console.log('emailFromServer>>>>>>>', thi.emailFromServer);
  //         }

  //         console.log('loginUserIdForMors', thi.loginUserIdForMors);

  //         thi.storage.set('loginUserIdForMors', JSON.parse(event.data));
  //         console.log('loginUserIdForMors', thi.loginUserIdForMors.CODE);

  //         if (connectedDevices.CODE) {
  //           console.log('morsc code exits');

  //           thi.getDevices();
  //         }
  //         // thi.sendNewOtp();
  //       }

  //       console.log(
  //         'connected  devices tabs page' + JSON.stringify(connectedDevices)
  //       );
  //       console.log(
  //         'deviceConsumptions  devices tabs page' +
  //           JSON.stringify(this.deviceConsumptions)
  //       );

  //       if (connectedDevices.CMD == 'DEVICELIST') {
  //         console.log('reached');
  //         thi.connectedDevices = connectedDevices;
  //         console.log('DEVICE LIST' + JSON.stringify(this.connectedDevices));
  //       } else if (connectedDevices.CMD == 'CONSUME') {
  //         // thi.service.deviceConsumptions = connectedDevices

  //         thi.deviceConsumptions.CMD = connectedDevices.CMD;
  //         thi.deviceConsumptions.LIST[0].DeviceName = connectedDevices.DeviceID;
  //         thi.deviceConsumptions.LIST[0].CON = connectedDevices.CON;
  //         thi.deviceConsumptions.LIST[0].TCON = connectedDevices.TCON;
  //         thi.deviceConsumptions.LIST[0].AMOUNT = connectedDevices.AMOUNT;
  //         thi.deviceConsumptions.LIST[0].PLUG = connectedDevices.PLUG;
  //         thi.deviceConsumptions.LIST[0].BALANCE = connectedDevices.BALANCE;
  //         // thi.deviceConsumptions.LIST[0].AMOUNT = connectedDevices.AMOUNT;
  //         thi.balance = thi.deviceConsumptions.LIST[0].BALANCE;

  //         console.log('CONSUME LIST' + JSON.stringify(thi.deviceConsumptions));
  //       }
  //       // thi.service.connectedDevices=thi.service.connectedDevices.LIST
  //     };
  //     this.ws.onerror = function (event) {
  //       console.log('error occured', event);
  //       this.isSocketConnected = false;
  //     };
  //     this.ws.onclose = function () {
  //       console.log('socket connection closed');
  //       this.isSocketConnected = false;
  //     };
  //   });

  //   return true;
  // }

  async startSocket() {
    console.log('Start Socket');

    // if(thi.LoginType == 'fb'){
    //   // thi.currentUser.email = this.currentUser.id
    //   this.emailValue = this.currentUser.id
    // }
    // else{
    //   this.emailValue = this.currentUser.email
    // }
    this.ws = new WebSocket('ws://13.126.140.93:4920');
    this.isSocketConnected = true;
    

    // this.ws = new WebSocket("ws://03.108.252.21:1010");
    // this.ws = new WebSocket("ws://13.234.110.18:1010");
    this.platform.ready().then(() => {
      var thi = this;
      //  this.socket.connect();

      // console.log("called show owner info", thi.currentUser.email);

      this.ws.onopen = function () {
        var obj: any = {
          CMD: 'REG',
          // DeviceID: "satyamchoudhary477@gmail.com",
          // DeviceID: thi.currentUser.email
          // ? thi.currentUser.email
          // : "satyamchoudhary477@gmail.com",
          // DeviceID: thi.emailValue,
          DeviceID: thi.deviceId,

          // DeviceID: "abc",
          NUMBER: JSON.stringify(thi.mobileNumber),
          Local: false,

          // DeviceID: thi.currentUser.email
          // ? thi.currentUser.email
          // : "greeniemax@gmail.com",
        };

        console.log('data ', JSON.stringify(obj));
        this.send(JSON.stringify(obj));

        // thi.getDevices();
        // thi.getList

        // setTimeout(() => {
        //   var obj1: any = {
        //     "CMD": "LOC", "LONG": "79.05241989999999", "LAT": "21.1051488", "RANGE": "100"
        //   }
        //   console.log("CMD LOC" + JSON.stringify(obj1))

        //   this.send(JSON.stringify(obj1))
        // }, 10)

        // setTimeout(() => {
        //   var obj2: any = {
        //     CMD: "LIST",
        //   };
        //   console.log("qr data" + JSON.stringify(obj2));
        //   this.send(JSON.stringify(obj2));
        // }, 10);
      };
      this.ws.onmessage = function (event) {
        console.log('response from server tabs' + '  ' + event.data);

        console.log('response from server tabs page data' + '  ' + event.data);

        var connectedDevices = JSON.parse(event.data);

        console.log('connectedDevices', connectedDevices);

        if (connectedDevices.CMD == 'OTP' && connectedDevices.OTP) {
          thi.otp = connectedDevices.OTP;
          // alert("otp is >>> " + thi.otp);
        }

        if (connectedDevices.CMD == 'REG') {
          thi.loginUserIdForMors = JSON.parse(event.data);

          console.log('loginUserIdForMors', thi.loginUserIdForMors);

          thi.storage.set('loginUserIdForMors', JSON.parse(event.data));
          console.log('loginUserIdForMors', thi.loginUserIdForMors.CODE);

          if (connectedDevices.CODE) {
            console.log('morsc code exits');

            thi.getDevices();
          }
          // thi.sendNewOtp();
        }

        console.log(
          'connected  devices tabs page' + JSON.stringify(connectedDevices)
        );
        console.log(
          'deviceConsumptions  devices tabs page' +
            JSON.stringify(this.deviceConsumptions)
        );

        if (connectedDevices.CMD == 'DEVICELIST') {
          console.log('reached');
          thi.connectedDevices = connectedDevices;
          console.log('DEVICE LIST' + JSON.stringify(this.connectedDevices));
        } else if (connectedDevices.CMD == 'CONSUME') {
          // thi.service.deviceConsumptions = connectedDevices

          thi.deviceConsumptions.CMD = connectedDevices.CMD;
          thi.deviceConsumptions.LIST[0].DeviceName = connectedDevices.DeviceID;
          thi.deviceConsumptions.LIST[0].CON = connectedDevices.CON;
          thi.deviceConsumptions.LIST[0].TCON = connectedDevices.TCON;
          thi.deviceConsumptions.LIST[0].AMOUNT = connectedDevices.AMOUNT;
          thi.deviceConsumptions.LIST[0].PLUG = connectedDevices.PLUG;
          thi.deviceConsumptions.LIST[0].BALANCE = connectedDevices.BALANCE;
          // thi.deviceConsumptions.LIST[0].AMOUNT = connectedDevices.AMOUNT;
          thi.balance = thi.deviceConsumptions.LIST[0].BALANCE;

          console.log('CONSUME LIST' + JSON.stringify(thi.deviceConsumptions));
        }
        // thi.service.connectedDevices=thi.service.connectedDevices.LIST
      };
      this.ws.onerror = function (event) {
        console.log('error occured', event);
        thi.isSocketConnected = false;
      };
      this.ws.onclose = function () {
        console.log('socket connection closed');
        thi.isSocketConnected = false;
      };
    });

    return true;
  }

  sendNewOtp() {
    console.log('Start Socket');

    var thi = this;

    var obj1: any = {
      CMD: 'NEWOTP',
      DeviceID: thi.deviceId,
      // DeviceName: "abcdedf",
      NUMBER: JSON.stringify(thi.mobileNumber),
    };

    console.log('device req', obj1);
    thi.ws.send(JSON.stringify(obj1));

    this.ws.onmessage = function (event) {
      console.log('response from server tabs' + '  ' + event.data);
      var res = JSON.parse(event.data);
      console.log('res', parseInt(res.OTP));
      if (res.CMD == 'OTP' && res.OTP) {
        thi.otp = parseInt(res.OTP);
      }
    };
  }

  async getDevices() {
    setTimeout(async () => {
      console.log('call get devices');
      var thi = this;
      var obj1: any = {
        CMD: 'LOC',
        LONG: '72.05241989999999',
        LAT: '19.1051488',
        RANGE: '100',
      };

      console.log('device req', obj1);
      thi.ws.send(JSON.stringify(obj1));
      thi.ws.onmessage = await function (event) {
        // console.log("event ", event.data);
        var response = JSON.parse(event.data);

        if (response.CMD == 'LOC') {
          console.log('getDevices logg>>>>>', response);

          thi.deviceListForHomePage = response.Devices;

          console.log('deviceListForHomePage befor', thi.deviceListForHomePage);

          thi.deviceListForHomePage.forEach((element) => {
            thi.reverseGeocode(element.LAT, element.LON).then((res) => {
              // console.log("ress>>>>", res);
              element['location'] = res;
            });
          });

          console.log('deviceListForHomePage after', thi.deviceListForHomePage);

          // thi.getListInStarting();
          thi.getUserOwnedDevices();
        }
      };
      return true;
    }, 500);
  }

  ////////////////////////////////// last function before navigating to homepage
  getListInStarting() {
    var thi = this;
    var obj: any = {
      CMD: 'LIST',
    };
    console.log('qr data' + JSON.stringify(obj));
    thi.ws.send(JSON.stringify(obj));

    thi.ws.onmessage = function (event) {
      console.log('response of getListInStarting ', event.data);
      let res = JSON.parse(event.data);
      if (res.CMD == 'DEVICELIST') {
        console.log('navigate to root');
        if (!thi.emailFromServer) {
          console.log('email does not exits');

          thi.getEmailFromUser();
        } else {
          thi.navCtrl.navigateRoot('/tabs');
        }
      }
    };
  }

  async getEmailFromUser() {
    // const alert = await this.alertCtrl.create({
    //   cssClass: "my-custom-class",
    //   header: "Enter Email Id!",
    //   backdropDismiss:false,
    //   inputs: [
    //     {
    //       name: "name1",
    //       type: "email",
    //       placeholder: "Ex : abc@gmail.com",
    //     },
    //   ],
    //   buttons: [
    //     {
    //       text: "Ok",
    //       cssClass: 'btnWarning',
    //       handler: (alt) => {
    //         console.log("Confirm Ok", alt.name1);
    //         if (alt.name1) {
    //           this.saveEmailOfUser(alt.name1);
    //         }
    //       },
    //     },
    //   ],
    // });

    // await alert.present();

    const modal = await this.modalController.create({
      component: GetEmailPage,
      backdropDismiss: false,
      cssClass: 'my-custom-modal-css',
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    console.log(data);
    console.log(JSON.stringify(data));

    if (data) {
      this.saveEmailOfUser(data.email);
    }
  }

  async saveEmailOfUser(data) {
    // if (/(.+)@(.+){2,}\.(.+){2,}/.test(data)) {
    console.log('saveEmailOfUser called', data);
    var thi = this;

    const obj: any = {
      CMD: 'EMAIL',
      EMAIL: data,
    };
    console.log('data to send for saveEmailOfUser ', JSON.stringify(obj));
    thi.ws.send(JSON.stringify(obj));
    thi.ws.onmessage = function (event) {
      console.log('saveEmailOfUser response', event.data);

      let res = JSON.parse(event.data);

      if (res) {
      } else {
        thi.showToast('Error while saving Email');
      }
    };
    // } else {
    //   this.showToast("Please enter a valid email id");
    //   this.getEmailFromUser();
    // }
  }

  scan() {
    console.log('scanning started');
    // this.presentLoading("Please Wait")

    var thi = this;
    this.barcodeScanner.scan().then((barcodeData) => {
      console.log('Barcode data', barcodeData);
      console.log('Barcode data text', barcodeData.text);

      thi.scanQrCode = barcodeData.text;
      thi.storage.set('QRCODE', thi.scanQrCode);
      console.log('qr code got after scanning' + thi.scanQrCode);

      var obj2: any = {
        CMD: 'QRCODE',
        // VALUE: thi.scanQrCode,
        VALUE: 'hello',
      };
      console.log('sending for scanning' + JSON.stringify(obj2));
      thi.ws.send(JSON.stringify(obj2));

      thi.ws.onmessage = function (event) {
        console.log('response from server after scanning' + '  ' + event.data);
        var deviceConsumptions = JSON.parse(event.data);
        console.log('device consumptions' + JSON.stringify(deviceConsumptions));
        thi.deviceConsumptions.CMD = deviceConsumptions.CMD;
        thi.deviceConsumptions.LIST[0].DeviceName = deviceConsumptions.DeviceID;
        thi.deviceConsumptions.LIST[0].CON = deviceConsumptions.CON;
        thi.deviceConsumptions.LIST[0].TCON = deviceConsumptions.TCON;
        thi.deviceConsumptions.LIST[0].AMOUNT = deviceConsumptions.AMOUNT;
        thi.deviceConsumptions.LIST[0].PLUG = deviceConsumptions.PLUG;
        // thi.deviceConsumptions.LIST[0].STATUS = deviceConsumptions.STATUS

        thi.deviceConsumptions.LIST[0].BALANCE = deviceConsumptions.BALANCE;
        // console.log("status" + thi.deviceConsumptions.LIST[0].STATUS)

        if (thi.deviceConsumptions.CMD == 'CONSUME') {
          thi.chargingStatus = 'CHARGING';
          thi.dismissLoading();
        }
        // thi.interval = thi.startInterval();

        console.log(
          'device consumptions after scanning tabs page',
          thi.deviceConsumptions
        );
      };
    });

    setTimeout(() => {
      this.getList();
    }, 100);
  }

  async plug() {
    console.log('start charge');

    var thi = this;
    var obj1: any = { CMD: 'PLUG', deviceID: 'GBD12C1', Plug: 2, State: 'ON' };
    thi.ws.send(JSON.stringify(obj1));
    thi.ws.onmessage = await function (event) {
      console.log('getDevices logg data>>>>>', event.data);
    };

    setTimeout(() => {
      this.getList();
    }, 100);
  }

  async verifyOtp(otp) {
    console.log('verify  otp', otp);

    var thi = this;
    var obj1: any = {
      CMD: 'OTP',
      OTP: otp,
      NUMBER: JSON.stringify(thi.mobileNumber),
    };
    console.log('request object of verify otp', obj1);

    return new Promise((resolve, reject) => {
      thi.ws.send(JSON.stringify(obj1));
      thi.ws.onmessage = function (event) {
        console.log('verify  otp>>>>>', event.data);

        // if (event.data.CMD == "REG" && event.data.CODE) {
        console.log(' before getStratedWithLogin');

        thi.getStratedWithLogin();
        // }

        resolve(event.data);
      };
    });
  }

  getStratedWithLogin() {
    console.log('getStratedWithLogin');

    var thi = this;
    this.ws.onmessage = function (event) {
      console.log('response from server tabs' + '  ' + event.data);

      console.log('response from server tabs page data' + '  ' + event.data);

      var connectedDevices = JSON.parse(event.data);

      if (connectedDevices.CMD == 'REG') {
        thi.loginUserIdForMors = JSON.parse(event.data);

        if (connectedDevices.ISOWNER == 'true') {
          console.log('is owner true');
          thi.isUser = false;
        } else if (connectedDevices.ISOWNER == 'false') {
          console.log('is owner false');
          thi.isUser = true;
        }

        console.log('isUser>>>', thi.isUser);

        console.log('loginUserIdForMors', thi.loginUserIdForMors);

        thi.storage.set('loginUserIdForMors', JSON.parse(event.data));
        thi.storage.set('currentUser', thi.mobileNumber);

        if (thi.loginUserIdForMors.EMAIL) {
          thi.emailFromServer = thi.loginUserIdForMors.EMAIL; //////////////////////////
        }

        if (connectedDevices.CODE) {
          console.log('morsc code exits');
          thi.getDevices();
          // thi.navCtrl.navigateRoot("/tabs");
        }

        console.log('loginUserIdForMors', thi.loginUserIdForMors.CODE);
      }

      console.log(
        'connected  devices tabs page' + JSON.stringify(connectedDevices)
      );
      console.log(
        'deviceConsumptions  devices tabs page' +
          JSON.stringify(this.deviceConsumptions)
      );

      if (connectedDevices.CMD == 'DEVICELIST') {
        console.log('reached');
        thi.connectedDevices = connectedDevices;
        console.log('DEVICE LIST' + JSON.stringify(this.connectedDevices));
      } else if (connectedDevices.CMD == 'CONSUME') {
        // thi.service.deviceConsumptions = connectedDevices

        thi.deviceConsumptions.CMD = connectedDevices.CMD;
        thi.deviceConsumptions.LIST[0].DeviceName = connectedDevices.DeviceID;
        thi.deviceConsumptions.LIST[0].CON = connectedDevices.CON;
        thi.deviceConsumptions.LIST[0].TCON = connectedDevices.TCON;
        thi.deviceConsumptions.LIST[0].AMOUNT = connectedDevices.AMOUNT;
        thi.deviceConsumptions.LIST[0].PLUG = connectedDevices.PLUG;
        thi.deviceConsumptions.LIST[0].BALANCE = connectedDevices.BALANCE;
        // thi.deviceConsumptions.LIST[0].AMOUNT = connectedDevices.AMOUNT;
        thi.balance = thi.deviceConsumptions.LIST[0].BALANCE;

        console.log('CONSUME LIST' + JSON.stringify(thi.deviceConsumptions));
      }
      // thi.service.connectedDevices=thi.service.connectedDevices.LIST
    };
  }

  async getUserOwnedDevices() {
    console.log('Get USer devices');
    var thi = this;

    var obj: any = {
      CMD: 'ISOWNER',
      DeviceID: thi.currentUser.email,
    };
    console.log('sending data to devices owned by user' + JSON.stringify(obj));
    thi.ws.send(JSON.stringify(obj));
    thi.ws.onmessage = async function (event) {
      // console.log("devices owned", event.data);

      thi.devicesOwned = JSON.parse(event.data);
      console.log('thi.devicesOwned', thi.devicesOwned);

      if (thi.devicesOwned.LIST.length != 0) {
        console.log('device list is not empty');
        thi.isUser = false;
        thi.devicesOwned = thi.devicesOwned.LIST;

        console.log('devicesOwned', thi.devicesOwned);

        await thi.devicesOwned.forEach((element) => {
          thi.reverseGeocode(element.LAT, element.LON).then((res) => {
            console.log('ress>>>>', res);
            element['location'] = res;
          });
        });
        console.log('devices at tabs 4' + JSON.stringify(this.devices));

        console.log('devicesOwned after', thi.devicesOwned);
        thi.storage.set('mydevices', thi.devicesOwned);
      } else {
        console.log('device list is empty');
        thi.isUser = true;
      }

      thi.getListInStarting();

      console.log('isUser', thi.isUser);
    };
  }

  getList() {
    console.log('get list called');

    var thi = this;

    setTimeout(() => {
      // var obj: any = {
      //   CMD: "LIST",
      // };
      // console.log("qr data" + JSON.stringify(obj));
      // thi.ws.send(JSON.stringify(obj));
      thi.ws.onmessage = function (event) {
        console.log(
          ' on message recieve event>>>>>>>>>>>>>>>>>>>>>',
          event.data
        );
        let connectedDevices = JSON.parse(event.data);

        console.log(
          'connected  devices tabs page' + JSON.stringify(connectedDevices)
        );
        console.log(
          'deviceConsumptions  devices tabs page' +
            JSON.stringify(this.deviceConsumptions)
        );

        if (connectedDevices.CMD == 'DEVICELIST') {
          console.log('reached');
          thi.connectedDevices = connectedDevices;
          console.log('DEVICE LIST' + JSON.stringify(this.connectedDevices));
        } else if (connectedDevices.CMD == 'CONSUME') {
          // thi.service.deviceConsumptions = connectedDevices

          console.log('connectedDevices', connectedDevices);

          var flag = false;

          if (thi.availableDevicessss == []) {
            thi.availableDevicessss.push(connectedDevices);
          } else {
            console.log('start foreach');
            thi.availableDevicessss.forEach((res) => {
              if (
                res.DeviceID == connectedDevices.DeviceID &&
                res.PLUG == connectedDevices.PLUG
              ) {
                console.log('if res>>>', res);
                // res = connectedDevices;
                res.CON = connectedDevices.CON;
                res.AMOUNT = connectedDevices.AMOUNT;
                res.BALANCE = connectedDevices.DUE;
                res.CMD = connectedDevices.CMD;
                res.DeviceID = connectedDevices.DeviceID;
                res.PLUG = connectedDevices.PLUG;
                res.TCON = connectedDevices.TCON;
                flag = true;
              }
              // res.showButton = true;
            });
            if (!flag) {
              console.log('pushing new one');
              thi.availableDevicessss.push(connectedDevices);

              thi.availableDevicessss.forEach((res) => {
                res.showButton = true;
              });
            }
            flag = false;
          }

          thi.availableDevicessss = [
            {
              CMD: 'CONSUME',
              PLUG: 1,
              DeviceID: 'XXXXX',
              CON: 0,
              TCON: 0,
              AMOUNT: 0,
              DUE: 'YYY',
              showButton: true,
            },
          ];

          console.log(
            'thi.availableDevicessss >>>>>>>>>>>>>>>',
            JSON.stringify(thi.availableDevicessss)
          );

          thi.chargingStatus = 'CHARGING';

          // thi.deviceConsumptions.CMD = connectedDevices.CMD;
          // thi.deviceConsumptions.LIST[0].DeviceName = connectedDevices.DeviceID;
          // thi.deviceConsumptions.LIST[0].CON = connectedDevices.CON;
          // thi.deviceConsumptions.LIST[0].TCON = connectedDevices.TCON;
          // thi.deviceConsumptions.LIST[0].AMOUNT = connectedDevices.AMOUNT;
          // thi.deviceConsumptions.LIST[0].PLUG = connectedDevices.PLUG;
          // thi.deviceConsumptions.LIST[0].BALANCE = connectedDevices.BALANCE;
          // thi.deviceConsumptions.LIST[0].AMOUNT = connectedDevices.AMOUNT;
          // thi.balance = thi.deviceConsumptions.LIST[0].BALANCE;

          // console.log(
          //   "CONSUME LIST >>>" + JSON.stringify(thi.deviceConsumptions)
          // );

          // thi.availableDevicessss = thi.deviceConsumptions.LIST;
        }
      };
    }, 10);
  }

  async reverseGeocode(lat, long) {
    // 19.3137989, 73.0656204

    // console.log("lat", lat + "long" + long);

    var geolist = [];
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5,
    };

    await this.nativeGeocoder
      .reverseGeocode(lat, long, options)
      .then((result: NativeGeocoderResult[]) => {
        geolist = result;
      })
      .catch((error: any) => console.log(error));

    var ans =
      geolist[0].subLocality +
      ' ' +
      geolist[0].locality +
      ' ' +
      geolist[0].subAdministrativeArea +
      ' ' +
      geolist[0].administrativeArea +
      ' ' +
      geolist[0].postalCode;

    return ans;
  }

  getUserHistory(DeviceName) {
    var thi = this;
    var obj: any = {
      CMD: 'HISTORY',
      DeviceID: DeviceName,
    };
    thi.ws.send(JSON.stringify(obj));
    thi.ws.onmessage = function (event) {
      console.log('user history', event.data);
      thi.userHistory = JSON.parse(event.data);
    };
  }

  async getUserBalance(month, year) {
    console.log('getUserBalance called');
    var thi = this;
    var obj: any = {
      CMD: 'MONEY',
    };
    console.log('cmd command for getUserBalance', JSON.stringify(obj));
    thi.ws.send(JSON.stringify(obj));
    thi.ws.onmessage = await function (event) {
      console.log('getUserBalance', event.data);
      thi.userBalance = JSON.parse(event.data);

      thi.userBalance = thi.userBalance.MONEY.toFixed(0);
      console.log('balance' + thi.userBalance);

      thi.getWalletHistory(month, year);
    };
  }

  async getWalletHistory(month, year) {
    this.walletHistory = {};
    console.log('getWalletHistory called');
    var thi = this;
    var obj: any = {
      CMD: 'REPORT-USER',
      MONTH: month,
      YEAR: year,
    };
    console.log('cmd command for getWallet History', JSON.stringify(obj));
    thi.ws.send(JSON.stringify(obj));
    thi.ws.onmessage = await function (event) {
      console.log('wallet history', event.data);
      thi.walletHistory = JSON.parse(event.data);

      console.log('wallet history', thi.walletHistory);

      thi.walletHistory.forEach((element) => {
        if (element.PriceSet) {
          element['price'] = (element.PriceSet * element.WhTotal) / 1000;
        }
      });

      console.log('wallet history', thi.walletHistory);

      if (thi.walletHistory.LIST) {
        // thi.walletHistory.LIST.forEach((element) => {
        //   if (element.info == "Stopped")
        //     element.Rate = element.Rate * element.TotalConsumption;
        // });
      }

      // if(thi.walletHistory){
      //   console.log("while returning",thi.walletHistory)

      // return thi.walletHistory
      // }
    };
  }

  addMoney(money) {
    console.log('addMoney called');
    var thi = this;
    var obj: any = {
      CMD: 'ADDMONEY',
      MONEY: money,
    };
    console.log('data to send for adding money', JSON.stringify(obj));
    thi.ws.send(JSON.stringify(obj));
    thi.ws.onmessage = function (event) {
      console.log('addMoney response', event.data);

      // thi.walletHistory = JSON.parse(event.data)
    };
  }

  installCharger(data) {
    console.log('install charger called');
    var thi = this;
    var obj: any = {
      CMD: 'INSTALL',
      DATA: {
        name: data.name,
        email: data.email,
        mobile: data.mobile,
        address: data.address,
      },
    };
    console.log('data to send for install charger', JSON.stringify(obj));
    thi.ws.send(JSON.stringify(obj));
    thi.ws.onmessage = function (event) {
      console.log('addinstall chargerMoney response', event.data);
      const res = JSON.parse(event.data);
      console.log('res', res);
      if (res.STATUS == 'SUCCESS') {
        if (res.MESSAGE) {
          thi.showToast(res.MESSAGE);
        }
        thi.location.back();
      }

      // thi.walletHistory = JSON.parse(event.data)
    };
  }

  async ClearDues(id) {
    console.log('clear dues called', id);
    var thi = this;
    var obj: any = {
      CMD: 'CLEAR-DUES',
      RazorID: id,
    };
    console.log('data to send for CLEAR-DUES', JSON.stringify(obj));
    thi.ws.send(JSON.stringify(obj));
    thi.ws.onmessage = await function (event) {
      console.log('CLEAR-DUES response', event.data);

      let res = JSON.parse(event.data);

      if (res.INFO == 'APPROVAL') {
        thi.openModal('success');
      } else {
        thi.openModal('fail');
      }
      // thi.walletHistory = JSON.parse(event.data)
    };
  }

  async openModal(data) {
    console.log('modal data', data);
    const modal = await this.modalController.create({
      component: SuccesAndFailComponent,
      backdropDismiss: true,
      swipeToClose: true,
      //  cssClass: "my-custom-modal-cssforDevice",
      cssClass: 'my-custom-modal-cssforTransaction',
      componentProps: { data: data },
      // componentProps: { data: 'success' },
    });
    await modal.present();
  }

  getHostReport(month, Year, deviceid) {
    console.log('addMoney called');
    var thi = this;
    var obj: any = {
      CMD: 'REPORT-HOST',
      deviceID: deviceid,
      Month: month,
      Year: Year,
    };
    console.log('data to send for report host', JSON.stringify(obj));
    thi.ws.send(JSON.stringify(obj));
    thi.ws.onmessage = function (event: any) {
      console.log('report host response', event.data);

      if (event.data) {
        // thi.deviceReportArray = [
        //   {
        //     _id: "61646808803aa1106c0e3b15",
        //     PhoneNumber: "9158161536",
        //     StartDateTime: "2021-10-11T16:36:24.498Z",
        //     Device: "9820F540",
        //     WhTotal: 1330,
        //     PriceSet: 20,
        //     ChargingStopped: true,
        //     Port: 1,
        //     EndDateTime: "2021-10-11T16:37:25.397Z",
        //   },
        //   {
        //     _id: "61647ca9247d2c0f406929ff",
        //     PhoneNumber: "9158161536",
        //     StartDateTime: "2021-10-11T18:04:25.849Z",
        //     Device: "9820F540",
        //     WhTotal: 2620,
        //     PriceSet: 20,
        //     ChargingStopped: true,
        //     Port: 1,
        //     EndDateTime: "2021-10-11T18:06:27.932Z",
        //   },
        //   {
        //     _id: "616989d8b03a2009e49c484e",
        //     PhoneNumber: "9158161536",
        //     StartDateTime: "2021-10-15T14:02:00.093Z",
        //     Device: "9820F540",
        //     WhTotal: 10,
        //     PriceSet: 20,
        //     ChargingStopped: true,
        //     Port: 1,
        //     EndDateTime: "2021-10-15T14:02:02.323Z",
        //   },
        //   {
        //     _id: "6169a1fab03a2009e49c4854",
        //     PhoneNumber: "9158161536",
        //     StartDateTime: "2021-10-15T15:44:58.816Z",
        //     Device: "9820F540",
        //     WhTotal: 10,
        //     PriceSet: 20,
        //     ChargingStopped: true,
        //     Port: 1,
        //     EndDateTime: "2021-10-15T15:45:01.125Z",
        //   },
        //   {
        //     _id: "6169a215b03a2009e49c4856",
        //     PhoneNumber: "9158161536",
        //     StartDateTime: "2021-10-15T15:45:25.606Z",
        //     Device: "9820F540",
        //     WhTotal: 10,
        //     PriceSet: 20,
        //     ChargingStopped: true,
        //     Port: 1,
        //     EndDateTime: "2021-10-15T15:45:27.944Z",
        //   },
        //   {
        //     _id: "616a690ab03a2009e49c485c",
        //     PhoneNumber: "9158161536",
        //     StartDateTime: "2021-10-16T05:54:18.651Z",
        //     Device: "9820F540",
        //     WhTotal: 10,
        //     PriceSet: 20,
        //     ChargingStopped: true,
        //     Port: 1,
        //     EndDateTime: "2021-10-16T05:54:22.310Z",
        //   },
        //   {
        //     _id: "616e80489de1c916d0395239",
        //     PhoneNumber: "9158161536",
        //     StartDateTime: "2021-10-19T08:22:32.726Z",
        //     Device: "9820F540",
        //     WhTotal: 1320,
        //     PriceSet: 20,
        //     ChargingStopped: true,
        //     Port: 1,
        //     EndDateTime: "2021-10-19T08:23:34.014Z",
        //   },
        //   {
        //     _id: "616e839d9de1c916d039523d",
        //     PhoneNumber: "9158161536",
        //     StartDateTime: "2021-10-19T08:36:45.422Z",
        //     Device: "9820F540",
        //     WhTotal: 5060,
        //     PriceSet: 20,
        //     ChargingStopped: true,
        //     Port: 1,
        //     EndDateTime: "2021-10-19T08:40:53.217Z",
        //   },
        //   {
        //     _id: "616e87c79de1c916d0395248",
        //     PhoneNumber: "9158161536",
        //     StartDateTime: "2021-10-19T08:54:31.760Z",
        //     Device: "9820F540",
        //     WhTotal: 70,
        //     PriceSet: 20,
        //     ChargingStopped: true,
        //     Port: 1,
        //     EndDateTime: "2021-10-19T08:54:34.132Z",
        //   },
        //   {
        //     _id: "616fbb9a9de1c916d0395273",
        //     PhoneNumber: "9158161536",
        //     StartDateTime: "2021-10-20T06:47:54.805Z",
        //     Device: "9820F540",
        //     WhTotal: 20,
        //     PriceSet: 20,
        //     ChargingStopped: true,
        //     Port: 1,
        //     EndDateTime: "2021-10-20T06:47:59.706Z",
        //   },
        // ];
        thi.deviceReportArray = JSON.parse(event.data);
        console.log('device list', thi.deviceReportArray);

        thi.totalPrice = 0;
        thi.totalConsumption = 0;

        thi.deviceReportArray.forEach((element) => {
          element.PriceSet = (element.WhTotal * element.PriceSet) / 1000;

          element['totalTime'] =
            new Date(element.EndDateTime).getTime() -
            new Date(element.StartDateTime).getTime();

          element.totalTime = Math.round(element.totalTime);

          let d = element.totalTime;
          var h = Math.floor(d / 3600);
          var m = Math.floor((d % 3600) / 60);
          var s = Math.floor((d % 3600) % 60);

          var hDisplay = h > 0 ? h + (h == 1 ? ' hour, ' : ' hours, ') : '';
          var mDisplay = m > 0 ? m + (m == 1 ? ' minute, ' : ' minutes, ') : '';
          var sDisplay = s > 0 ? s + (s == 1 ? ' second' : ' seconds') : '';

          element.totalTime = hDisplay + ' ' + mDisplay + ' ' + sDisplay;

          thi.totalPrice += element.PriceSet;
          thi.totalConsumption += element.WhTotal;
        });
      }

      // thi.walletHistory = JSON.parse(event.data)
    };
  }

  getWifiList(device) {
    console.log('get Wifi List');

    var thi = this;
    var obj: any = { CMD: 'WIFI-LIST', deviceID: device };
    console.log('data to send for getWifiList ', JSON.stringify(obj));

    return new Promise((resolve, reject) => {
      resolve({
        CMD: 'WIFI-DEVICE',
        PHONE: '9158161536',
        LIST: [
          {
            Network: 'RM',
          },
          {
            Network: 'mukhtar',
          },
          {
            Network: 'JioPrivateNet',
          },
        ],
        deviceID: '9820F540',
      });
      thi.ws.send(JSON.stringify(obj));
      thi.ws.onmessage = function (event) {
        console.log('get Wifi List response', event.data);
        var res = JSON.parse(event.data);
        console.log('res ', res);
        if (res.CMD == 'WIFI-DEVICE' && res.LIST) {
          // resolve(res);
          // thi.getWifiList = res.LIST
        }
      };
    });
  }

  switchWifi(ssid, pass, id) {
    console.log('get Wifi List');

    var thi = this;
    var obj: any = {
      CMD: 'WIFI-SET',
      SSID: ssid,
      PASS: pass,
      deviceID: id,
    };
    console.log('data to send for switchWifi ', JSON.stringify(obj));

    return new Promise((resolve, reject) => {
      thi.ws.send(JSON.stringify(obj));
      thi.ws.onmessage = function (event) {
        console.log('get  switchWifi response', event.data);
        var res = JSON.parse(event.data);
        console.log('res ', res);
        // if (res.CMD == "WIFI-DEVICE" && res.LIST) {
        //   resolve(res.LIST);
        // }
      };
    });
  }

  changePrice(money, deviceId) {
    console.log('addMoney called');
    var thi = this;
    var obj: any = {
      CMD: 'PRICENEW',
      deviceID: deviceId,
      VALUE: parseFloat(money),
    };
    console.log('data to send for changePrice', JSON.stringify(obj));
    thi.ws.send(JSON.stringify(obj));
    thi.ws.onmessage = function (event) {
      console.log('changePrice response', event.data);

      var res = JSON.parse(event.data);

      if (res.CMD == 'PRICENEW' && res.ANS == 'OK') {
        console.log('success');
        thi.getUserOwnedDevices();
        thi.location.back();
      } else if (res.ANS == 'OPERATIONAL' && res.CMD == 'PRICENEW') {
        thi.showToast('Device is in Operation. Try Again Later');
        thi.location.back();
      }
      // thi.walletHistory = JSON.parse(event.data)
    };
  }

  async stopCharging(data) {
    console.log('stop data', data);
    var thi = this;

    thi.availableDevicessss.forEach((res) => {
      console.log('devices data', res + 'data' + data);
      if (res.DeviceID == data.DeviceID && res.PLUG == data.PLUG) {
        res.showButton = false;
      }
    });

    var obj1: any = {
      CMD: 'PLUG',
      deviceID: data.DeviceID,
      Plug: data.PLUG,
      State: 'OFF',
    };
    thi.ws.send(JSON.stringify(obj1));
    thi.ws.onmessage = await function (event) {
      console.log('getDevices logg data>>>>>', event.data);

      var result = JSON.parse(event.data);

      console.log('result', result);

      if (result.DeviceStatus == 'Command Successful') {
        thi.chargingStatus = 'STOPPED';
      }

      // thi.getList();
    };

    // console.log("QR Code" + thi.scanQrCode);

    // var obj: any = {
    //   CMD: "PHONESTOP",
    //   QRCODE: thi.scanQrCode,
    //   // "QRCODE": thi.qrcodeData
    // };
    // console.log("data to stop charging ", JSON.stringify(obj));

    // thi.ws.send(JSON.stringify(obj));

    // thi.ws.onmessage = function (event) {
    //   console.log("charging stopped", event.data);
    //   var stoppedData = JSON.parse(event.data);

    //   console.log("stopped data" + JSON.stringify(thi.deviceConsumptions));
    //   thi.deviceConsumptions.CMD = stoppedData.CMD;
    //   thi.deviceConsumptions.LIST[0].DeviceName = stoppedData.DeviceID;
    //   thi.deviceConsumptions.LIST[0].TP1CON = stoppedData.TP1CON;
    //   thi.deviceConsumptions.LIST[0].AMOUNT = stoppedData.AMOUNT;

    //   thi.chargingStatus = "STOPPED";
    //   console.log("chrging status" + thi.chargingStatus);
    //   console.log("interval after stopping" + thi.interval);
    // };
  }

  async showToast(msg: string) {
    const toast = await this.toastController.create({
      message: msg,

      cssClass: 'toast-error',
      position: 'bottom',
      duration: 3000,
    });
    toast.present();
  }
  async presentLoading(text) {
    // show loading
    this.isLoading = true;
    return await this.loadingCtrl
      .create({
        message: text,

        spinner: 'bubbles',
      })
      .then((a) => {
        a.present().then(() => {
          console.log('presented');
          if (!this.isLoading) {
            a.dismiss().then(() => console.log('abort presenting'));
          }
        });
      });
  }
  async dismissLoading() {
    this.isLoading = false;
    return await this.loadingCtrl
      .dismiss()
      .then(() => console.log('dismissed'));
  }

  async logout() {
    console.log('called');
    const alert = await this.alertCtrl.create({
      header: 'Logout',
      cssClass: 'my-custom-class',
      message: '<strong>Are you sure?</strong>',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'Yes',
          cssClass: 'btnWarning',
          handler: () => {
            console.log('google logout');
            this.presentLoading('Please Wait');

            this.storage.clear().then(() => {
              this.dismissLoading();
              console.log('Confirm Okay');

              this.navCtrl.setDirection('root');
              this.router.navigateByUrl('/login');
            });
          },
        },
      ],
    });

    alert.present();
  }
}
// export class socket {
//   static ws: any = new WebSocket("ws://13.234.110.18:1010")
// }
